layout: md-base.j2
title: Access Forbidden

Oops. You don't have permission to access this resources. Here are a few reasons that might explain why.

* For current courses, solutions are usually restricted to enrolled students. If you're sitting in on the course and want access then contact me. (You will need an Andrew ID).

* For past courses, solutions are not available to students.

If none of the above apply, and you believe you reached this page due to a technical error, please contact me and I'll look into it.
