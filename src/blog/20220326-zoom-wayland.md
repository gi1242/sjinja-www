title: Sharing your screen with Zoom under Wayland
summary: Screen sharing works fine when you run Zoom on `xorg`. However, on `wayland` Zoom's screen sharing features don't work anymore. Here are a few methods to make it work.

## Preferred Method: Using OBS and Zoom's "Share content from 2nd camera" feature

1. Install [OBS Studio](https://obsproject.com), [V4L2 Loopback](https://github.com/umlaeute/v4l2loopback) and [XDG Desktop Portal](https://github.com/flatpak/xdg-desktop-portal).
   On Arch Linux you can do this by installing the packages `obs-studio`, `v4l2loopback-utils`, `xdg-desktop-portal` (and possibly `v4l2loopback-dkms` and `linux-headers` or `linux-lts-headers` as appropriate).
   Look [here](https://wiki.archlinux.org/title/Open_Broadcaster_Software) for more info.

2. Load the kernel module with `modprobe -iv v4l2loopback` (as root).
   To ensure it's always loaded on startup create the file `/etc/modules-load.d/v4l2loopback-local.conf` and put a line in it containing `v4l2loopback`.

3. Optionally you can also name the virtual camera by creating the file `/etc/modprobe.d/v4l2-loopback-local.conf` with contents:

        options v4l2loopback card_label="V4L2 Loopback Virtual Cam"

4. Start `obs`. If it  pops up the wizard, say you're only going to be using the virtual camera. My preferred settings are to choose the base canvas to be your screen size (or half the screen size if you have a 4K display), and the output resolution equal to the base canvas resolution. I chose a framerate of 20fps, but you mileage may vary.

5. Go to sources and add a source with *"Window capture (Pipewire)"*. If you don't see this option you may have to install [Pipewire](https://pipewire.org), and the right backend for `xdg-desktop-portal`.

6. Select the window you want to share and start the virtual camera in OBS. (If you don't see an option to do this, you may have to load the `v4l2loopback` module.)

7. Go to your Zoom meeting, share screen, advanced and click *Content from 2nd Camera*. Click switch camera (if necessary) until your desired screen content is shown.

8. If you want to change the window you're sharing, you don't have to change anything in Zoom. Just go to OBS, sources, select your Window Capture source, click settings and select the window you want.

**Note:** Most of the guides online suggested changing your primary camera in Zoom to the virtual camera from OBS. Then create different scenes in OBS with your shared window content, web cam, etc. and switching scenes in OBS when you want to share content. This method has some advantages (easily switch scenes with hotkeys, effects, etc.), but has one major downside: If your Zoom meeting doesn't have HD video enabled (or sometimes you're calling into a Zoom meeting in a different region), then your shared screen content will be extremely pixelated and essentially unreadable. You can avoid this using the share content from second camera feature in Zoom.

**Note:** Under Gnome/Wayland Zoom seems to have many glitches (popup windows don't appear, some buttons / settings are not clickable). One workaround is to launch Zoom with the `xcb` backend as follows:

    :::shell
    QT_QPA_PLATFORM=xcb zoom

## Method 2: Using a web browser

If you start a Zoom meeting from a browser, you can share content directly. (Tested under Firefox). While it's simpler to do, the video quality is worse, some features are missing, it uses more CPU, and is a bit laggy on my system.

## Method 3: Using Zoom under XOrg

Log out. Before logging back in your display manger should have options on what session to launch. Choose either KDE/Gnome under Xorg, and log in. Now Zoom screen sharing *should* work; but you'll lose a few Wayland features (proper scaling with multiple displays, swipe gestures, etc.)

If you anticipate doing this regularly, create a separate user to login under Xorg. Then you need Zoom's screen sharing features.
