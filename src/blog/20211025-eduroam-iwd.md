title: Connecting to eduroam on Linux (at Carnegie Mellon)
summary: Instructions on connecting to `eduroam` (and `CMU-SECURE`) at Carnegie Mellon on Linux, using `iwd`.

Somehow the posted instructions on connecting to `eduroam` or other WPA-EAP
networks never work. But some small tweak works 🙄. Here's what it took to get
`eduroam` and `CMU-SECURE` working for me at CMU using `iwd`:

## Connecting to eduroam using iwd

Create `/var/lib/iwd/eduroam.8021x`:

    [Security]
    EAP-Method=PEAP
    EAP-Identity=anonymous@andrew.cmu.edu
    EAP-PEAP-Phase2-Method=MSCHAPV2
    EAP-PEAP-Phase2-Identity=USERNAME@andrew.cmu.edu

    [Settings]
    AutoConnect=true

Now `iwd` will connect to `eduroam`.

## Connecting to CMU-SECURE

The only difference is to use

    :::cfg
    EAP-PEAP-Phase2-Identity=USERNAME

instead of `USERNAME@andrew.cmu.edu`.

## Using NetworkManager

If you want to use `NetworkManager`, then set it to use `iwd` as a back-end.
Edit `/etc/NetworkManager/NetworkManager.conf` to include:

    [device]
    wifi.backend=iwd

The [Arch Linux Wiki](https://wiki.archlinux.org/title/NetworkManager#Using_iwd_as_the_Wi-Fi_backend) has a lot more info.

## Not prompting for a password

The above will prompt you for a password every time you need to connect. If you
want to avoid that you can add the line

    :::cfg
    EAP-PEAP-Phase2-Password=your-password

to the `[Security]` section in `eduroam.8021x`. This stores your password in
clear text, which is obviously not ideal. You can alternately store an `md4`
hash of your password as follows:

1. Temporarily disable the history feature of your shell. On `zsh` you can do
   this by

        :::shell
        $ HIST_NO_STORE=1

2. Get the `md4` hash:

        $ echo -n "your-password" | iconv -t utf16le | openssl md4
        (stdin)= ba15efd8f2cb03eb12b34998a35133cc

    Copy the password hash (everything after the equals sign) for use in the
    next step. 

3. Add the following line to the `[Security]` section in `eduroam.8021x`:

        :::cfg
        EAP-PEAP-Phase2-Password-Hash=(paste password hash from above)
