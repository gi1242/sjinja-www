name: GI
avatar: https://cdn.libravatar.org/avatar/1d9eff8253e59f5c7c243a8c558e4f98c0b2d3df3533dc05a5da7adffc798be2
subject: Switched to git
date: 2015-09-11 23:19:32

Looks like they now switched to [git](http://git-scm.com/). Clone with
```
git clone https://github.com/5digits/dactyl.git
```
