page: 20171208-bibtex-custom-label.md
subject: Re: Indentation
name: Gautam Iyer
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2021-09-15 12:51:04 EDT
ip: 73.154.129.255
referer: https://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

So at the beginning of the `.bib` file produced by BibTeX theres a `\begin{thebibliography}{XXX12}`. Change this to `\begin{thebibliography}{LONGESTLABEL}` and you should be good.
