page: 20171208-bibtex-custom-label.md
subject: great
name: Anonymous
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2018-10-10 06:05:12 EDT
ip: 130.83.116.25
referer: http://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

Thanks for the great work man! I used this on the german version of alpha, geralpha, and it worked like a charm. 
Had been stuck on the problem of unusual reference labeling that my research institute requires for a while now, and this solved the problem in a few minutes.
