page: 20150807-box-webdav.md
subject: mount cant find
name: MRE
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2016-07-14 07:49:34 EDT
ip: 133.82.251.185
referer: http://www.math.cmu.edu/~gautam/sj/blog/20150807-box-webdav.html

on step 3:
nano /etc/fstab
add the line as written, save and exit
at step 7, I get back "mount: can't find username/net/box in /etc/fstab or /etc/mtab
I have tried a number of different versions of the same guide, and always get trapped at the same place.
