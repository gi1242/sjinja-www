page: 20140310-zathura-fsearch.md
subject: Re: And on vim ?
name: Gautam Iyer
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2020-02-15 12:13:59 EST
ip: 128.2.112.77
referer: http://www.math.cmu.edu/~gautam/sj/blog/20140310-zathura-fsearch.html

Hi Laurant,

Good to hear from you. Hope you're doing well.

> Your config works fine on gVim but not on Vim (set `synctex-editor-command gvim +%{line} %{input}`). Do you happen to know a way to set it up on basic Vim (for some weird reasons I prefer not to use gVim) ?

I actually use it myself on basic vim, and don't like using gvim either. I
fixed the links to the scripts `szathura` and `svim`. If you use those,
you should be able to get it working with a console vim.

Best,

Gautam
