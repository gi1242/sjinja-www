page: 20171208-bibtex-custom-label.md
subject: Re: Wrong link?
name: Gautam Iyer
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2018-01-29 15:03:46 EST
ip: 128.2.117.145
referer: http://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

Oops. You're right! I uploaded a new version now. Thanks for pointing it out.
