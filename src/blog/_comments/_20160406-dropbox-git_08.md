page: 20160406-dropbox-git.md
subject: Dropbox Exclude
name: Gautam Iyer
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2021-08-06 09:45:02 EDT
ip: 127.0.0.1
referer: http://localhost:8000/blog/20160406-dropbox-git.html

Initially I recommended excluding the `.git` file from dropbox using `dropbox exclude` or the selective sync feature. As of today (Aug 2021), this doesn't work for me on Linux. If I exclude a file, dropbox prevents a file with that name being created locally. It's simpler to just tell your collaborators to not modify that file...
