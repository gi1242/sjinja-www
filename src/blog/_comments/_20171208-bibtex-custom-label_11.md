page: 20171208-bibtex-custom-label.md
subject: Re: Indentation
name: Gautam Iyer
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2021-09-17 21:23:33 EDT
ip: 73.154.129.255
referer: https://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

Indeed I agree that's better; I took a quick look at the file and it looks like it should already be doing that. But it's not... I'll see if I can chase down the error soon
