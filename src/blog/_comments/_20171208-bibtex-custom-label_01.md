page: 20171208-bibtex-custom-label.md
subject: Wrong link?
name: Anonymous
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2018-01-29 05:59:38 EST
ip: 137.226.12.101
referer: http://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label

Thanks for your great work. As far as I can tell, the referenced BST file 20171114-bibtex-doi/halpha-abbrv.bst does not contain the necessary changes.
