page: 20150324-bibtex-reverse.md
subject: How do we change the style of bibliography while maintaining the reverse chronological order?
name: Nilah Nair
email: nilah.nair@tu-dortmund.de
avatar: https://cdn.libravatar.org/avatar/b014e8df3db9f55c5be505adb59c05a6
date: 2021-12-15 10:04:25 EST
ip: 129.217.193.183
referer: https://www.math.cmu.edu/~gautam/sj/blog/20150324-bibtex-reverse.html

I would like to use an author, year style like that of agsm. but i want the bibliogrpahy to be sorted in reverse chronological order. Would that be possible? Could you please guide me as to where is the change required?
