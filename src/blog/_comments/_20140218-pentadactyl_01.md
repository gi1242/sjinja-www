name: GI
avatar: https://cdn.libravatar.org/avatar/1d9eff8253e59f5c7c243a8c558e4f98c0b2d3df3533dc05a5da7adffc798be2
subject: Official download now available
date: 2014-06-22 16:22:15

As of 2014-03-15, it seems that an official download is available [here](http://5digits.org/pentadactyl/).
So far this seems to work fine with `iceweasel-24.6.0esr-1~deb7u1`.
(`xul-ext-pentadactyl-1.1+hg7904-0+nmu1` currently in `debian/testing` wants `iceweasel` 25 or later which is not yet (2014-06-22) in `wheezy/updates`.)
