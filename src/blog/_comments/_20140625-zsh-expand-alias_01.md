page: 20140625-zsh-expand-alias.md
subject: A little question
name: Anonymous
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2019-12-22 06:26:53 EST
ip: 223.74.5.104
referer: http://www.math.cmu.edu/~gautam/sj/blog/20140625-zsh-expand-alias.html

How to understand this statement `"\<(${(j:|:)ealiases})\$"`?
