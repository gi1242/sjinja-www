ip: 67.186.56.112
subject: Toggle Proxy
date: 2014-03-23 01:58:17

Once your proxies are set up the firefox addon [Toggle Proxy](https://addons.mozilla.org/en-US/firefox/addon/toggle-proxy-51740) allows you to toggle between them painlessly.
