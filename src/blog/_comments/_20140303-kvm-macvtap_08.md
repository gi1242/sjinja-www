page: 20140303-kvm-macvtap.md
subject: Re: problem with setting the private NAT not default
name: GI
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2018-03-24 17:41:51 EDT
ip: 24.3.21.34
referer: http://www.math.cmu.edu/~gautam/sj/blog/20140303-kvm-macvtap.html

The `ip route` command you want is

    ip route change default via x.x.x.1 dev eth0

Here `x.x.x.1` is the ip address of the gateway on eth0 (which I'm assuming is your public macvtap interface). Usually this is the IP address of the interface with the last digit replaced by 1.

This won't persist across reboots. My suggestion to get something permanent is to figure out what DHCP client your system is using and configure it to not set the default route on your NAT interface. A few other common clients are `dhcpcd`, `pump` and `udhcpc`. Try seeing if one of these is installed and running on your system, and then look up the appropriate configuration instructions.
