page: 20171208-bibtex-custom-label.md
subject: Re: Indentation
name: Love
email: love.client@gmail.com
avatar: https://cdn.libravatar.org/avatar/29c3d3c8d5743f0b2b8748e07121d015
date: 2021-09-16 05:36:39 EDT
ip: 195.249.127.231
referer: https://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

That works, thank you. However, it would be more satisfying not to have to do this manually :-) Could your bst-file be modified to take into account the lengths of the added labels?
