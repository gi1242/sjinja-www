page: 20150807-box-webdav.md
subject: Mount command fails with "500 Internal Server Error" on Unbuntu
name: Don
email: donbarr@rogers.com
avatar: https://cdn.libravatar.org/avatar/1ef79ed22bfa3b06e9e797273e309b88
date: 2017-11-15 11:53:42 EST
ip: 99.239.44.26
referer: http://www.math.cmu.edu/~gautam/sj/blog/20150807-box-webdav.html

On Ubuntu 4.4.0-98-generic hitting "500  Internal Server Error" when trying to mount Box

    sudo mount /home/username/Box
    Please enter the username to authenticate with server
    https://dav.box.com/dav or hit enter for none.
    Username: donbarr@ca.ibm.com
    Please enter the password to authenticate user donbarr@ca.ibm.com with server
    https://dav.box.com/dav or hit enter for none.
    Password:  
    /sbin/mount.davfs: Mounting failed.
    500 Internal Server Error

Used the same credentials going directly to https://dav.box.com/dav in a browser -- I get no error but the screen stays blank.  So not clear if the passthru to Box isn't working or something else.
