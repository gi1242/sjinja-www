page: 20171208-bibtex-custom-label.md
subject: Indentation
name: Love Lindholm
email: love.client@gmail.com
avatar: https://cdn.libravatar.org/avatar/29c3d3c8d5743f0b2b8748e07121d015
date: 2021-09-15 11:32:18 EDT
ip: 195.249.127.231
referer: https://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

Hello!

Thank you for your BibTeX style!

I have an issue with indentation: If my custom label is above a certain limit, the text of the BibTeX entry corresponding to that label starts with an indentation, i.e., it is not aligned with the start of the text for entries with shorter labels. 

Is this something you have noticed? I am not TeX-savvy enough to modify your bst-file.

Kind regards
Love
