page: 20140301-git-latexdiff.md
subject: Re: It may have issues when colored text is already in tex file
name: Gautam Iyer
email: gautam@math.cmu.edu
avatar: https://cdn.libravatar.org/avatar/0ec534a31b72b69338d0897211ec9925
date: 2021-08-12 16:26:48 EDT
ip: 164.52.231.106
referer: https://www.math.cmu.edu/~gautam/sj/blog/20140301-git-latexdiff.html

Yes, many complex changes break `latexdiff`, and the resulting file won't compile. Mostly I make revisions without any highlighting; then if the `-diff` file complies cleanly, I send the PDF to co-authors / journal. If it doesn't, I use

    git wdiff @~1 | aha > changes.html

and then send the `changes.html` file to my co-authors.
