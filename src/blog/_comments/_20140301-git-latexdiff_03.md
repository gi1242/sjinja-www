page: 20140301-git-latexdiff.md
subject: Re: Re: It may have issues when colored text is already in tex file
name: Dr.Feng
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2021-10-09 03:16:41 EDT
ip: 204.124.181.174
referer: https://www.math.cmu.edu/~gautam/sj/blog/20140301-git-latexdiff.html

Hi, Prof. Iyer. Thank you for the reply.  Unfortunately, journals in my area topically require that changes be highlighted for revision/resubmission. But your answer offers an alternative to latexdiff. Thanks.
