page: 20140310-zathura-fsearch.md
subject: And on vim ?
name: Laurent Dietrich
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2020-02-15 06:30:40 EST
ip: 89.159.91.128
referer: http://www.math.cmu.edu/~gautam/sj/blog/20140310-zathura-fsearch.html

Hi Gautam ! I don't know if you remember me, I was at postdoc at the CNA a few years ago. Fun fact : I just found this page, trying to set up forward and backward sync with vim :) 

Your config works fine on gVim but not on Vim (set synctex-editor-command "gvim +%{line} %{input}"). Do you happen to know a way to set it up on basic Vim (for some weird reasons I prefer not to use gVim) ?
 
Thanks a lot, hope you're doing well at CMU.
