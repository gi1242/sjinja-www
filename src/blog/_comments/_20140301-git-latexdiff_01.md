page: 20140301-git-latexdiff.md
subject: It may have issues when colored text is already in tex file
name: Dr.Feng
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2021-08-11 23:51:42 EDT
ip: 58.63.131.120
referer: https://www.math.cmu.edu/~gautam/sj/blog/20140301-git-latexdiff.html

Hi Prof. Iyer. Thanks for sharing the detailed instruction. I tried this when doing a paper revision. For revision, I already had some text highlighted with color, such as `\textcolor{blue}{foo bar}`, and that is because revision typically requires changes being highlighted. In this case, when compiling the resulted `-diff.tex` file, errors occurs. Have you encountered issues similar to this? Thanks.
