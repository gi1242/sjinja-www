page: 20171208-bibtex-custom-label.md
subject: Nice, but labels aren't sorted alphanumerically
name: Anonymous
email: unknown@email.id
avatar: https://cdn.libravatar.org/avatar/99daad327e93a79b9b1a785c70649146
date: 2019-10-30 07:00:45 EDT
ip: 89.3.3.106
referer: http://www.math.cmu.edu/~gautam/sj/blog/20171208-bibtex-custom-label.html

The style is very nice. One gripe: I have labels such as `RFC1` and `RFC2`, which I'd expect to appear in that order. However, the order is reversed when the latter is cited before the former. (Also, `\bibliographystyle{habbrv}` should be `\bibliographystyle{halpha-abbrv}`, or `halpha-abbrv.bst` should be renamed.)
