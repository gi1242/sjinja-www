title: Using bibtool to fix your BibTeX databases.
tags: latex
summary: Downloading [BibTeX](http://www.bibtex.org/) citations from review sites (e.g. [MathSciNet](http://www.ams.org/mathscinet), [WebOfKnowledge](http://apps.webofknowledge.com/)) doesn't always get you consistent results. Sometimes the case is messed up, there are extra braces, etc. Or there is a `note` field showing the number of citations (which shows up in your final PDF output). Moreover the citation keys are often of the form `MR673830`, which is useless unless you're a computer. Here's an easy way to fix most of these problems using [BibTool](http://www.gerd-neugebauer.de/software/TeX/BibTool/).

{{summary}}

Here's how you use it:

1. Create a `~/.bibtoolrsc` file with your preferences.
   You can edit [[{{filesdir}}/bibtoolrsc.txt|my preferences]] to suit your taste if you like.

2. Filter your bibliography through bibtool:

	    bibtool < downloaded_refs.bib > cleaned_up_refs.bib

    Or, if you use [vim](http://www.vim.org) you can just do

	    :1,$!bibtool

    when editing your bibliography file.

3. That's it. You might have to fix the case of a few words in the title here and there (e.g. writing `{G}alton-{W}atson` instead of `Galton-Watson`, etc.).
