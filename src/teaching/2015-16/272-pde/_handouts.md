* [[schedule.md|Lecture Schedule]]
* [372 Lecture notes (wiki)](http://wiki.math.cmu.edu/iki/2014-372/)
* [372 website (Spring 2014)](http://www.math.cmu.edu/~gautam/teaching/2013-14/372-pde/)
* Previous 372 exams:
    * [Midterm 1](http://www.math.cmu.edu/~gautam/teaching/2013-14/372-pde/pdfs/midterm1.pdf) and [[auth/372-2014-midterm1-sol.pdf|solutions]]
    * [Midterm 2](http://www.math.cmu.edu/~gautam/teaching/2013-14/372-pde/pdfs/midterm2.pdf) and [[auth/372-2014-midterm2-sol.pdf|solutions]]
    * [Final](http://www.math.cmu.edu/~gautam/teaching/2013-14/372-pde/pdfs/final.pdf) and [[auth/372-2014-final-sol.pdf|solutions]]
* Your [[pdfs/midterm1.pdf|Midterm 1]] and [[auth/midterm1-sol.pdf|solutions]]
* Your [[pdfs/midterm2.pdf|Midterm 2]] and [[auth/midterm2-sol.pdf|solutions]]
* Your [[pdfs/final.pdf|final]] and [[auth/final-sol.pdf|solutions]]
