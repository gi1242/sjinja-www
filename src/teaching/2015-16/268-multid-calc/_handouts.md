* Very Brief Notes
    * [[pdfs/ch1-limits.pdf|Limits]]
    * [[pdfs/ch2-diff.pdf|Differentiation]]
    * [[pdfs/ch3-inv.pdf|Inverse and Implicit Functions]]
    * [[pdfs/ch4-int.pdf|Multiple integrals]]
    * [[pdfs/ch5-lint.pdf|Line Integrals]]
    * [[pdfs/ch6-sint.pdf|Surface Integrals]]
    * [[pdfs/brief-notes.pdf|All chapters]]
      (and [TeX Source](https://gitlab.com/gi1242/cmu-math-268))
* [[pdfs/2012-notes.pdf|Lecture notes (2012)]]
  (and [TeX Source](https://gitlab.com/gi1242/cmu-math-268))
* [[pdfs/sample-questions.pdf|Sample Midterm 1 Questions]]
* Your [[pdfs/midterm1.pdf|Midterm 1]] and [[auth/midterm1-sol.pdf|Solutions]]
* [[pdfs/sample-questions2.pdf|Sample Midterm 2 Questions]]
* Your [[pdfs/midterm2.pdf|Midterm 2]] and [[auth/midterm2-sol.pdf|Solutions]]
* [[pdfs/sample-questions3.pdf|Sample integration Questions]]
* Your [[pdfs/final.pdf|Final]] and [[auth/final-sol.pdf|Solutions]]
