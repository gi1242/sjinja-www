## Course Description

This course is a serious introduction to multidimensional calculus that makes use of matrices and linear transformations.
Students will be expected to write proofs; however, some of the deeper results will be presented without proofs.

### Tentative Syllabus

* Functions of several variables, regions and domains, limits and continuity.
* Partial derivatives, linearization, Jacobian.
* Chain rule, inverse and implicit functions and geometric applications.
* Higher derivatives, Taylor's theorem, optimization, vector fields.
* Multiple integrals and change of variables, Leibnitz's rule.
* Line integrals, Green's theorem.
* Path independence and connectedness, conservative vector fields.
* Surfaces and orientability, surface integrals.
* Divergence theorem and Stokes's theorem.

### Textbook and References

* **Textbook:**
  *Advanced Calculus (5th Edition)* by Wilfred Kaplan.
  (<span class='bg-warning'>Homework problems will be assigned from this book.</span> I will cover roughly chapters 2 through 5.)
* *Vector Calculus, Linear Algebra, and Differential Forms: A Unified Approach* by Hubbard and Hubbard.
* *Multivariable Mathematics: Linear Algebra, Multivariable Calculus, and Manifolds* by Shifrin.

## Class Policies

### Lectures

* If you must sleep, <span class='text-danger'>don't snore!</span>
* Be courteous when you use mobile devices.

### Homework

* Homework must be turned in **at the beginning of class** on the due date.
* <span class='text-danger lead'>Late homework will **NEVER** be accepted. Really!</span>
* To account for unusual circumstances, the bottom 20% of your homework will not count towards your grade.
* Working in groups is encouraged, but solutions must be written up on your own.
* Nearly perfect student solutions may be scanned and hosted here, with your identifying information removed. If you don't want any part of your solutions used, please make a note of it in the margin of your assignment.


### Exams

* All exams are closed book, in class.
* No calculators, computational aids, or internet enabled devices are allowed.
* The final time will be announced by the registrar
  [here](https://www.cmu.edu/hub/courses/exams/index.html).
  <span class='text-danger'>
  Be aware of their schedule before making your travel plans.
  </span>

### Grading

* Homework will count as 20% of your grade.
* <span class='text-danger'>Between 25% and 50% of your exams will consist of homework questions.</span>
* Your *better* midterm will count as 30% of your grade.
  (Your *worse* midterm will not count at all.)
* Your final will count as 50% of your grade.
