{%- from get_file('course_info.j2') import course -%}
title: Lecture Schedule
breadcrumb: index.html|{{course.title}}

Here is a lecture by lecture list of topics covered in class.

## Functions of several variables <small>(Lectures 1 -- ?, 8/31 -- ?)</small>

*Reference: Kaplan, Chapter 2*

* Open sets and domains
* Limits
* Continuity
* Directional and Partial Derivatives
* Derivative (or "Total Differential")
* Tangent planes
* Chain rule
* Higher order derivatives
* Maxima/Minima
