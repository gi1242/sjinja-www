## Course Description

This course is a serious introduction to multidimensional calculus that makes use of matrices and linear transformations.
Students will be expected to write proofs; however, some of the deeper results will be presented without proofs.

### Tentative Syllabus

* Functions of several variables, regions and domains, limits and continuity.
* Partial derivatives, linearization, Jacobian.
* Chain rule, inverse and implicit functions and geometric applications.
* Higher derivatives, Taylor's theorem, optimization, vector fields.
* Multiple integrals and change of variables, Leibnitz's rule.
* Line integrals, Green's theorem.
* Path independence and connectedness, conservative vector fields.
* Surfaces and orientability, surface integrals.
* Divergence theorem and Stokes's theorem.

### Textbook and References

There are plenty of references on Calculus and can be divided into isomorphism classes based on difficulty. (Translation: I'm not expanding my brief notes.)

* My brief lecture notes: for [[pdfs/notes.pdf|printing]] or for [[pdfs/notes-tablet.pdf|viewing online]].
* [Khan Academy](https://www.khanacademy.org/math/multivariable-calculus). (Lots of examples, pictures, intuition; but not at the level of rigor that will be expected in this course.)
* Hermann, Strang Calculus Volume 3. (At a level a bit lower than this course; but available for free on [OpenStax](https://openstax.org/subjects/math).)
* Lecture notes by Santiago [[pdfs/canez-calculus.pdf|Canez]] (also available on his [website](http://www.math.northwestern.edu/~scanez/courses/320/notes/lecture-notes-320-3.pdf). (A bit deeper / more through than we will have time for in this course.)
* *Advanced Calculus of Several Variables* by C. H. Edwards, Jr (roughly chapters 2 through 5; again at a level slightly higher than we will have time for in this course.)
* *Advanced Calculus (5th Edition)* by Wilfred Kaplan.
  (roughly chapters 2 through 5).
  **Note:** When I initially recommended this book, it used to be a cheap paperback book.
  It is not so cheap anymore, and I do not recommend buying it.
* *Multivariable Mathematics: Linear Algebra, Multivariable Calculus, and Manifolds* by Shifrin.
* The more analytically inclined can also use any of the references used for 269 [any of the references used for 269](../../2015-16/268-multid-calc/index.html#textbook-and-references)

## Class Policies

### Lectures

* If you must sleep, <span class='text-danger'>don't snore!</span>
* Be courteous when you use mobile devices.

### Homework

* Homework must be turned in **at the beginning of class** on the due date.
* **Late homework policy:**
    - Late homework will *NOT* be accepted.
      In particular, homework turned in **after class starts** will NOT be accepted.
    - To account for unusual circumstances, the bottom 20% of your homework will not count towards your grade.
    - I will only consider making an exception to the above late homework policy if you have documented personal emergencies lasting at least **18 days**.
* I recommend starting the homework early.
  Most students will not be able to do the homework in one evening.
* I view homework more as a learning exercise as opposed to a test.
  Feel free to collaborate, use books and whatever resources you can find.
  I recommend trying problems independently first, and then seeking help on problems you had trouble with.
* I also strongly urge you to fully understand solutions before turning them
in.
  I will usually put a few homework problems on your exams *with a devious twist*.
  A through understanding of the solutions (even if you didn't come up with it yourself) will invariably help you.
  But knowledge of the solution without understanding will almost never help you.
* Nearly perfect student solutions may be scanned and hosted here, with your identifying information removed.
  If you don't want any part of your solutions used, please make a note of it in the margin of your assignment.


### Exams

* All exams are closed book, in class.
* No calculators, computational aids, or internet enabled devices are allowed.
* The final time will be announced by the registrar
  [here](https://www.cmu.edu/hub/courses/exams/index.html).
  <span class='text-danger'>
  Be aware of their schedule before making your travel plans.
  </span>

### Grading

* Homework will count as 20% of your grade.
  Moreover, <span class='text-danger'>between 25% and 50% of your exams will consist of (possibly modified) homework questions,</span> so I advise you to really understand the homework.
* The remainder 80% of your grade will be determined by your exams weighted as the *higher* of:
    - 20% for each midterm, and 40% final,
    - **or** 30% for your *better* midterm and 50% for the final,
* If you miss a midterm for some reason, I **will not** give you a makeup.
  Instead, I will count your other midterm as 30% and final as 50% using the second option above.
