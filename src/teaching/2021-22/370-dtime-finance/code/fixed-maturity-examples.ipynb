{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are a few examples on pricing securities with a fixed maturity time. This code is mainly for illustrative purposes, and not optimized to run well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "%precision 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fix a few parameters for all examples.\n",
    "u = 1.05\n",
    "d = .9\n",
    "S0 = 10\n",
    "r = .03\n",
    "N = 90\n",
    "\n",
    "# Risk neutral probabilities\n",
    "p = (1 + r - d) / (u-d)\n",
    "q = (u - 1 -r) / (u - d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Price a European Call option with strike K\n",
    "K = 100\n",
    "g = lambda s: max(s-K, 0) # Payoff of the call\n",
    "\n",
    "# Choose our state process Y = S. First find Range(Y_n) with a forward in time loop.\n",
    "# Then find f_n(Y_n) with a backward in time loop.\n",
    "\n",
    "# In python range is a pre-defined function, so we use dom to denote the Range(Y_n)\n",
    "dom = empty( N+1, dtype=object )\n",
    "dom[0] = set( [S0] ) # Assumes S0 < U\n",
    "for n in range(N):\n",
    "    dom[n+1] = set()\n",
    "    for s in dom[n]:\n",
    "        dom[n+1].add( d*s )\n",
    "        dom[n+1].add( u*s )\n",
    "        \n",
    "# Now dom[n] is Range(S_n). Let's compute f by backward induction.\n",
    "# It's convenient to define a function that computes the AFP at time n, given the AFP at time n+1\n",
    "# This is often called the \"rollback operator\" in numerical libraries\n",
    "def Rn(n, s):\n",
    "    return (f[n+1][u*s]*p + f[n+1][d*s]*q) / (1+r)\n",
    "\n",
    "# Now run a loop backwards to compute f[n] starting from n = N\n",
    "f = empty( N+1, dtype=object )\n",
    "f[N] = { s:g(s) for s in dom[N] }\n",
    "for n in range(N-1, -1, -1):\n",
    "    f[n] = { s: Rn(n, s) for s in dom[n] }\n",
    "\n",
    "# That's it, now print a few values of the AFP\n",
    "\n",
    "print( f'f[0]={f[0]}' )\n",
    "print( f'f[1]={f[1]}' ) # Should print a list of prices, depending on the stock price"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look simple? Well, there are a few traps we shouldn't fall for.\n",
    "For instance, we know range(S_n) has n+1 elements in it.\n",
    "Let's see how many elements are in our computed $\\operatorname{range}(S_n)$ for $n=30$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(dom[30])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Woops. I got 283 on my system. I should have gotten 31! What went wrong?\n",
    "\n",
    "Turns out it's floating point errors. Mathematically $u(u^m d^n) = u^{m+1} d^n$ for any $u, d \\in \\mathbb R$. On computers this is false, due to round off errors. Let's check. (Note in Python $u^m$ is written as `u**m`.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u**2 * d**5 == u*(u * d**5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That gave false on my system, even though mathematically the identity is true...\n",
    "\n",
    "How do you avoid this? There are a few ways. Here's a suggestion for one workaround: Instead of storing the floating point number $u^i d^j$ inside `dom[n]`, why don't we just store the integers $(i, j)$. (Or even just $i$, since $j$ can be worked out from $i$ and $n$.) This will avoid all floating point errors. See if you can do this (or find a different approach that avoids these floating point errors).\n",
    "\n",
    "If you can't find a way to do this, no worries: the floating error you make is super small (about $10^{-17}$). So the price of your security will likely be correct to a few decimal places. The real cost is the run time! Due to round off errors, `dom[n]` will grow much faster than it should. So if we wanted $N = 1000$, say, this algorithm may not finish as quickly as you would like. Try setting $N = 1000$ above for instance. (Good code will handle that instantly; bad code may not finish...)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Lets price an up and out call option, with the same strike price and upper barrier 500\n",
    "# If the stock price ever exceeds or equals U, the option is worthless. If not, its a regular call.\n",
    "K = 100\n",
    "U = 500"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We had an algorithm to price this in class. Choose the state process $Y = (S, M)$, where $M$ is the running maximum, and write the price in the form $V_n = f_n( S_n, M_n)$ and use the recurrence relation\n",
    "$$\n",
    "f_{n}(s,m) = \\frac{1}{1+r} \\bigl( \\tilde p f_{n+1}(us, m \\vee (us) ) + \\tilde q f_{n+1}(ds, m \\vee (ds) \\bigr)\n",
    "$$\n",
    "To code this up, note that if $m \\geq U$, the price is always $0$. So We only need to find the price when $m < U$. We also only need to keep track of the stock price when it is below $U$. Turns out, we won't even have to keep track of the range of $M$, and can simplify the above to\n",
    "$$\n",
    "f_n(s) = \\frac{1}{1+r} \\bigl(\n",
    "    \\tilde p f_{n+1}(us) 1_{us < U} + \\tilde q f_{n+1}(ds) 1_{ds < U}\n",
    "    \\bigr)\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note: I'm going to use the same bad code as above. It's a bit easier to read, but it is still\n",
    "# susseptable to floating point errors, and will choke if N is too large.\n",
    "\n",
    "\n",
    "# Find the range(Y_n) first.\n",
    "dom = empty( N+1, dtype=object )\n",
    "dom[0] = set( [S0] ) # Assumes S0 < U\n",
    "for n in range(N):\n",
    "    dom[n+1] = set()\n",
    "    for s in dom[n]:\n",
    "        if d*s < U: dom[n+1].add( d*s )\n",
    "        if u*s < U: dom[n+1].add( u*s )\n",
    "            \n",
    "            \n",
    "# Compute price. f[n][s] gives the price at time n when stock price is s,\n",
    "f = empty( N+1, dtype=object )\n",
    "f[N] = { s:max(s - K, 0) for s in dom[N] } # Payoff if M < U\n",
    "def Rn(n, s):\n",
    "    # Rollback operator\n",
    "    return (  p*(f[n+1][u*s] if u*s < U else 0 )\n",
    "            + q*(f[n+1][d*s] if d*s < U else 0 )\n",
    "           ) / (1+r)\n",
    "for n in range(N-1, -1, -1):\n",
    "    f[n] = { s: Rn(n, s) for s in dom[n] }\n",
    "    \n",
    "print( f'f[0]={f[0]}' )\n",
    "print( f'f[1]={f[1]}' ) # Should print a list of prices, depending on the stock price"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
