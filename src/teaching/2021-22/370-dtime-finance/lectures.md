title: Videos of Lectures, and Notes

These are PDFs of everything I wrote on the screen during lectures, along with links to videos for every lecture that the recording worked.
The materials from office hours from your TA's are [[auth/gdrive.md|here]].

{% set videos = dict(
  l01='https://cmu.zoom.us/rec/share/ToRy10rY66W58aAEFQ1YV1vLpKlsWG6cnbtxic0Jug-FauOp9kSqNYB4C_waVqrM.eT3qW8FY3y_WJISM',
  l02='https://cmu.zoom.us/rec/share/_ZOIrqiB9RAvzZGmfJSbp1Qau46HDzh4mCfBEa07ZfXU0__QoHA-UL-uMl_FK9r7.8RMaD2tp_bS90rBA',
  l03='https://cmu.zoom.us/rec/share/74tJumzwkL_JtMXYAo_s0Zx2SvxD_ihHSPjDvm2Jiuf8ceD1j-WGzz2dpa82a14.Lb1ZOk8M9UEBRIjy',
  l04='https://cmu.zoom.us/rec/share/pDG7XpuyA6C0i_Aig11EdoKvVcqJd7d_AP5qTGeAaGejQdxHlod1kg9gqC0Jm5rd.fR0tUuK3EsxexlFM',
  l05='https://cmu.zoom.us/rec/share/DyxnofMPaywNUXEIJlgN1JIiYA9AQyFbin_A-Uj1an46oCUKavUCN9RFabmuf8-y.T-aoJk6SpiZDjb-j',
  l06='https://cmu.zoom.us/rec/share/P9-oscWJ3X-r1GDyk7nSuBhuhbL67ACGqZ3MaoL49RLCDkI5K-Pk4bfFl6vgtwrX.AJ7mthE0r0Ec6aGl',
  l07='https://cmu.zoom.us/rec/share/uiDePazTCI0PoswcHhScfA7lLr13RpKs0GqE59HEZ23AWAg6mg0lnOGG1uXk4eUF.FR5cj7-yXEctvbHX',
  l08='https://cmu.zoom.us/rec/share/eF1f-MODY9-IFR_oaWNzzKf8IXAQ0epOYAB3R7G7hX3Dr6PgJocy5pdBfaeNAGvc.3zilTlH5qFFscvk-',
  l09='https://cmu.zoom.us/rec/share/ylXwLLwIR2XOMpOsI8FL9VJGBZSmnqj9klmlaBrDXktGzaVf0z5N5X7LRrj_ARxk.mPYr_MJ_5ZW2K77T',
  l10='https://cmu.zoom.us/rec/share/nfowDEgopXGBlImlP7Sljd7I-GYXNymQSr-egwkdFGo69qGg7Em_JHIOjbAfAR-S.n13ZiXgKzMs2kFro',
  l11='https://cmu.zoom.us/rec/share/wMNTW1X2-eTFNCwa6cFME8srdLEIBT-h7d6YxGkJLXxL7oJVBQca2kQn4Uf_k5M.2l4arik-j7xzvHbj',
  l12='https://cmu.zoom.us/rec/share/5B7XSviwDRvOp3hfxsOe8iOwOc25uQnUjq7Ue0Voiey_3xero6XQpRmpFthDw3Fa.ZS7aM95jpKCN0nVa',
  l13='https://cmu.zoom.us/rec/share/yPGXPuK3pq4AiMRwkI_SzXdQRjLo-DZHc2GC2MLUp9ew7WeqzheES5t_5_XXfne2.MUdpgHIWa2Dizc20',
  l14='https://cmu.zoom.us/rec/share/7OhOoEHBtjsrV3WWl3ZZYAedvCANbeds8gXy75Kos2p5KJOe9eVeFJP7OXy4pT35.WFX-OqoovVOPH0Mg',
  l15='https://cmu.zoom.us/rec/share/SeM6-v3cM8YAFPQozxVS-kc9t4sinWo9KUe7FPr1VX--yuZRcakP4vLq2tNwygAr.S9Sax9WxOg_2ZrFa',
  l16='https://cmu.zoom.us/rec/share/aA170BCH9Og5-L8Hie5kOP_fWxyqy6ayC8jpWKNQDIADDjjWZZDTS_nkE2i0YQCC.U6dTVC4G3XKZ_x9s',
  l17='https://cmu.zoom.us/rec/share/OXN7igUt26zpQpJ8V1ofCpOOGi0ucs5LewTBI-6YKBawNBuXUJ_7qbqsqvMbhWHH._VDSbJk40CVsgFwg',
  l18='https://cmu.zoom.us/rec/share/y3bj-Zth5OdZ7Iy5Ercyg_mXohmJZbcn_gmABl7wlAXdMkE3rnoEprk-79LticZJ.C81w89a6BUZvQ_n-',
  l19='https://cmu.zoom.us/rec/share/8R3XllZk5xXuNP_HL1-MO5cWklQB8m8cRJ_jDeV4POkrRP_LyjpIe4HfoMlG2vUY.sDZuE2wSUm6mCtWT',
  l20='https://cmu.zoom.us/rec/share/NJ3P1n2NyCJaMQ97QNkUupx2iRLIRFGGxBo0t9ktSKKtvgF8ussXTPUBGuZtO5AH.zRHFGQqjC7h44JZ5',
  l21='https://cmu.zoom.us/rec/share/zR6_znkXMYjbw8fN9je6uNc8uFEXB3AracZhv7gOOAkn54Z7fjSwfgobQx9z8UQW.EbsX2rVMuxVb1CcY',
  l22='https://cmu.zoom.us/rec/share/5bq6_S59BQ_EhkCjKnIBfpKBq--eGQfBR2JJ-B0bZdFR9pm1lSMZj1-8Chl9mNyp.CIapvB5ppEfIVMST',
  l23='https://cmu.zoom.us/rec/share/NmJqFnxMD5uG1-ESXQEUnRfKje9dmKipLl2t_52Zrato9vU_BOm98PWVPSMQ4a1M.-i3JqUTSKIqE7Stc',
  l24='https://cmu.zoom.us/rec/share/QE0q9ufNCqd3SjxSs0vZqYroLgLYf6xwHhbhvu5XEUyEOA8mLlpQEjmwnBlKbl18.vTYJ0Gy8sSjHIocJ',
  l25='https://cmu.zoom.us/rec/share/57hZVEWirdO7XACbJfE6lm2NE00lr1DjNt43iGaq0NnwnC7NYkkRd2rfu_y6yRD_.hfrpiPKqjprsSLkU',
  l26='https://cmu.zoom.us/rec/share/fgzupwrmyig0IxWpZEnDBfyRnncG9owa6fdAHa4k8OtDf6U7Y5oBpwKpZkPlgFIv.3-NCmux60ITlyyL4',
  l27='https://cmu.zoom.us/rec/share/afppELp0AE0UgyCpiFpFY0mvHmVKgeebh-9fcoGryQo3YVnvwSmM92aalwPEsc6x.wdH2VQcImdtDMxz0',
  l28='https://cmu.zoom.us/rec/share/nOBrJPfQ9MF36HjFJov2e8m5ZTrXmfh2ilBjp-6c6qwdqkeGQqjn_iClHtXTdKN2.aOKVcaP6eCGdcHpM',
  l29='https://cmu.zoom.us/rec/share/thiKNqSRHH3g0VcRic17udVLTto6_QpExd08qPV2v8RzkUjcj3mRaaG5K5Fieyhq.q3yG9nKCSMQONgmj',
  l30='https://cmu.zoom.us/rec/share/AICLg2Dx2F8nzta9EZxSA4z0xjn6Ws_Pn9v0dInklagRTxwsIQlcL3w4mXVN63oc.nOTdYGtNcwxXqSah',
  l31='https://cmu.zoom.us/rec/share/NiJ0CkQFZQP2Lz1xRt3UzCz79sKemxkbtLLlpQQHvQafTMtHpeDVOMSClsF7auKC.T7KcJq7VLU4WdY-D',
  l32='https://cmu.zoom.us/rec/share/bquHR1TEk89TsYmvz6aC-9MbKGpiNcaoSKTIQmtCJgWP6z1Ci8rLxf5UU2ookZmf.LN-3byicZViYHOLz',
  l33='https://cmu.zoom.us/rec/share/59a2KpycrB6NUb8vj-oSPAy4aKbgiRXjrG4nCljhvhs3esHQ_idpMTlvmv6bfDRR.ZGTg8608rNCqepTd',
  l34='https://cmu.zoom.us/rec/share/Cha8LD7QLIPsr0v2wdK7BOSOLG_rEZpe0Y3x-NBGJk_KX8VoUwY8dZYyyMSbK_4.M-G36O-1IkW_VNTa',
  l35='https://cmu.zoom.us/rec/share/K_WozjVo29_Ra4-tX61fvIEUI1EvmafioA6FHBgWScoewr4pIfg73wp5nxPRa0aT.fcL00jqc6BGWjP3b',
  l36='https://cmu.zoom.us/rec/share/UX8scBeLUhMk7sxcHLV0rhu6aD3p4MRvLC3koW4QSRL_5FFNGzF7ZZkN_hcCY3ub.3uqsjX7JXG_AvH1r',

  o08='https://cmu.zoom.us/rec/share/dfP7p3PkdUJ8jefzDgzHbYvQIfowt1DT1kmFNWPgfE7wpsfRAEo7Dz1dR2QcU6u0.az2THmAgDAigOYBQ',
  o18='https://cmu.zoom.us/rec/share/FIt_acmZiWiHL0DFYNgXNxq5lE0pW03xW9iugABSlQlydMClHskq1AMUjcT3yk2H.oeMyVTsIs6ck3xnC',
) -%}
{% for f in glob('pdfs/lec/20*-?[0-9]*.pdf') | sort -%}
  {% set fn = f | replace( 'pdfs/lec/', '') | replace( '.pdf', '') -%}
  {% set date = fn[0:8] -%}
  {% set type = fn[9:] -%}
  {% if loop.first -%}
    <table class='table'>
      <thead>
        <tr>
          <th scope='col'>Date</th>
          <th scope='col'>Event</th>
          <th scope='col'>PDF</th>
          <th scope='col'>Video</th>
        </tr>
      </thead>
  {% endif -%}
      <tbody>
        <tr>
          <td>{{date[0:4]}}-{{date[4:6]}}-{{date[6:8]}}</td>
          <td>
            {%- if type[0] == 'r' -%}
              Recitation
            {%- elif type[0] == 'o' -%}
              Office hour
            {%- else -%}
              Lecture
            {%- endif -%}
            {{' #' ~ type[1:]}}
          </td>
          <td><a href='{{get_link(f)}}'>🗐</a></td>
          {%- if videos[type] %}
            <td><a href='{{videos[type]}}'>🎥</a></td>
          {%- endif %}
        </tr>
  {% if loop.last -%}
      </tbody>
    </table>
  {% endif -%}
{% else -%}
    * *Videos and notes will be posted as the semester progresses.*
{% endfor %}

<div class='alert alert-warning small'>
  <b>Note:</b> If the video of the most recent lecture isn't online by 10:30pm on the day of class, then there is most likely an error I'm not aware of. Please email me.
</div>
