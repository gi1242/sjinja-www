title: Numerically pricing securities

Code I've written in connection to this class will be posted here.
You are free to copy and modify it to suit your purposes.

All code I post will be in [Python](https://www.python.org/) as a [Jupyter Notebook](https://jupyter.org/) using [NumPy](https://numpy.org/) (numerical python libraries).
To run my code you will have to install these on your computer.
The websites above have installation instructions.

You are, of course, free to write code in your preferred language, or even use a spreadsheet.
My code is provided here just as an example.

* Pricing  a few fixed maturity options.
  [[code/fixed-maturity-examples.html|view]]
  [[code/fixed-maturity-examples.ipynb|download]].
* Pricing up-rebate options using the state process $Y = S$:
  [[code/up-rebate-options.html|view]]
  [[code/up-rebate-options.ipynb|download]].
{#
* Pricing up-rebate options using the state process $Y = (S, M)$, where $S$ is the stock price, and $M$ is the running maximum:
  [[code/up-rebate-options.html|view]]
  [[code/up-rebate-options.ipynb|download]].
* Pricing American/European options (much faster code; but less general as it pre-supposes the domain):
  [[code/american-options.html|view]]
  [[code/american-options.ipynb|download]].
* A vote forecasting model:
  [[code/vote-forecasting.html|view]]
  [[code/vote-forecasting.ipynb|download]].
#}
