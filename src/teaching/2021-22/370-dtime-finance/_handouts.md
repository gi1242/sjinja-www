{%- from 'teaching/courses.j2' import old_teaching -%}
* [[auth/zoom.md]]
<div class='my-2'></div>
* [[lectures.md]]
{# * [[pdfs/dtime-fin-today.pdf|Todays lecture (unannotated)]] -#}
* All slides from class:
    - [[pdfs/dtime-fin-ann.pdf|Annotated]]
    {# - [[pdfs/dtime-fin-spaced.pdf|Unannotated]] -#}
    - [[pdfs/dtime-fin-compressed.pdf|Unannotated compactified]]
    - [LaTeX source](https://gitlab.com/gi1242/cmu-math-370)
* [[auth/gdrive.md]]
* [[auth/2019-kramkov-notes.pdf|2019 notes by Kramkov]]
<div class='my-2'></div>
* [[code.md]]
<div class='my-2'></div>
* [[../../2020-21/370-dtime-finance/pdfs/midterm1.pdf|2020 Midterm 1]] and [[auth/2020-midterm1-sol.pdf|solutions]].
* Your [[pdfs/midterm1.pdf|Midterm 1]] and [[auth/midterm1-sol.pdf|solutions]].
* [[../../2020-21/370-dtime-finance/pdfs/midterm2.pdf|2020 Midterm 2]] and [[auth/2020-midterm2-sol.pdf|solutions]].
* Your [[pdfs/midterm2.pdf|Midterm 2]] and [[auth/midterm2-sol.pdf|solutions]].
* [[../../2020-21/370-dtime-finance/pdfs/final.pdf|2020 final]] and [[auth/2020-final-sol.pdf|solutions]].
* Your [[auth2/final.pdf|Final]] and [[auth2/final-sol.pdf|Solutions]]
{#
* [[auth/midterm2.md|Solutions to Midterm 2]].
<div class='my-2'></div>
-#}
<div class='my-2'></div>
* [[../../2020-21/370-dtime-finance/index.md|Old 370 website (2020)]]
