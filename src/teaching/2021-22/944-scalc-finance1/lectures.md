title: Videos of Lectures, and Notes

These are PDFs of everything I write on the document camera during lectures.
Videos of in person lectures will be on Canvas/MediaSite.
Other videos recorded on Zoom (if any) will be posted here.

{% set videos = dict(
  l99='https://cmu.zoom.us/rec/share/ToRy10rY66W58aAEFQ1YV1vLpKlsWG6cnbtxic0Jug-FauOp9kSqNYB4C_waVqrM.eT3qW8FY3y_WJISM',

  o08='https://cmu.zoom.us/rec/share/dfP7p3PkdUJ8jefzDgzHbYvQIfowt1DT1kmFNWPgfE7wpsfRAEo7Dz1dR2QcU6u0.az2THmAgDAigOYBQ',
) -%}
{% for f in glob('pdfs/lec/20*-?[0-9]*.pdf') | sort -%}
  {% set fn = f | replace( 'pdfs/lec/', '') | replace( '.pdf', '') -%}
  {% set date = fn[0:8] -%}
  {% set type = fn[9:] -%}
  {% if loop.first -%}
    <table class='table'>
      <thead>
        <tr>
          <th scope='col'>Date</th>
          <th scope='col'>Event</th>
          <th scope='col'>PDF</th>
          <th scope='col'>Video</th>
        </tr>
      </thead>
  {% endif -%}
      <tbody>
        <tr>
          <td>{{date[0:4]}}-{{date[4:6]}}-{{date[6:8]}}</td>
          <td>
            {%- if type[0] == 'r' -%}
              Recitation
            {%- elif type[0] == 'o' -%}
              Office hour
            {%- else -%}
              Lecture
            {%- endif -%}
            {{' #' ~ type[1:]}}
          </td>
          <td><a href='{{get_link(f)}}'>🗐</a></td>
          {%- if videos[type] %}
            <td><a href='{{videos[type]}}'>🎥</a></td>
          {%- endif %}
        </tr>
  {% if loop.last -%}
      </tbody>
    </table>
  {% endif -%}
{% else -%}
    * *Notes will be posted as the semester progresses.*
{% endfor %}

<div class='alert alert-warning small'>
  <b>Note:</b> If the notes of the most recent lecture aren't online by 10:30pm on the day of class, then there is most likely an error I'm not aware of. Please email me.
</div>
