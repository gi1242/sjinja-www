## Course Description

The main focus of this course is the understanding of the tools used to price securities in continuous time finance.
This includes conditional expectation, martingales, Brownian motion, Itô integrals and Itô's formula, exponential martingales, and the Girsanov theorem.
The course will cover the Black-Scholes option pricing model in detail, and may touch upon the fundamental theorems of asset pricing.

### Learning Objectives

* Understand the basics of arbitrage free pricing.
* Develop an understanding of and familiarity with the mathematical tools used to price derivative securities in continuous time.

### Prerequisites

* [MSCF Probability Prep](https://www.cmu.edu/mscf/academics/curriculum/mscf-probability-prep.html)
* [MSCF Math Prep](https://www.cmu.edu/mscf/academics/curriculum/mscf-math-prep.html)

### References

* [[pdfs/notes.pdf|Brief lecture notes]].
  (A tablet friendly version is [[pdfs/notes-tablet.pdf|here]], and the full TeX source is [here](https://gitlab.com/gi1242/cmu-mscf-944).)
* [[auth/2020-kramkov-mpap.pdf|Slides]] by Dima Kramkov from Multi-period asset pricing in 2020.
* [[auth/bill-binom-black-scholes.pdf|Special Lectures]]  by Bill Hrusa on going from the Binomial model to Black-Scholes.
* [MSCF videos](https://canvas.cmu.edu/courses/3194/discussion_topics) on *Probability* and *Stochastic Calculus Self Study*.
* Stochastic Calculus for Finance Volumes I and II, by Steven Shreve.
{#- *(We will do the basics of the multi-period binomial model from volume I, and cover roughly the first five chapters of volume II.)* #}

## Class Policies

### Lectures

* If you must sleep, <span class='text-danger'>don't snore!</span>
* Be courteous when you use mobile devices.
{#-
* **Attendance Requirement:** The steering committee has requested attendance be recorded and made a part of your grade.
  Accordingly, attendance will count as 5% of your overall grade, and will be computed as follows:
    - To account for interviews and other special circumstances, you may miss up to 4 lectures without penalty.
    - Missing more than 4 lectures will decrease the attendance portion of your grade proportionally.
    - I will only consider making exceptions to this policy for unexpected severe emergencies that require your absence for more than 12 days.
#}

### Homework

* All homework must be scanned and turned in via [[auth/gradescope.md|Gradescope]].
* Please take good quality scans; homework that's too hard to read won't be graded. I recommend using a good scanning app that adjusts the contrast of your images for readability. (I use Adobe Scan myself).
* **Late homework policy:**
    - Any homework turned within the first hour of the deadline will be assessed a 20% penalty. (Note, it takes a few minutes to upload your homework/exam via [[auth/gradescope.md|Gradescope]]. So don't cut it too close to the deadline.)
    - Any homework turned after the first hour past the deadline will be assessed a 100% penalty (i.e. you won't receive credit for this homework, but if practical, your homework may still be graded).
    - To account for unusual circumstances, you may turn in one assignment up to 24 hours late without penalty, and you may also skip one homework assignments.
      That is, I will not assess a late penalty on the one assignments you turn in at most 24 hours late, and I will drop the bottom score on your homework from your grade.
    - I will only consider making an exception to the above late homework policy if you have documented personal emergencies lasting at least **12 days**.
    - Following MSCF policy, I will not make exceptions on account of job interviews or career fares.
* Solutions will usually be posted 48 hours after the homework deadline.
  Due to holidays or exams solutions to some assignments might post earlier.
  In this case, you will be notified of this in advance, and late homework **will not** be accepted after solutions have been posted.
* You may collaborate, use books and whatever resources you can find online in order to do the homework.
  However, you **must** write your solution up independently, **and** you must fully understand any solution you turn in.
  Turning in solutions you don't understand will be treated as a violation of academic integrity.

### Exams

* No notes, calculators, computational aids, or internet enabled devices are allowed during exams.
* You may not give or receive assistance during exams.
* Violation of these policies will be treated seriously according to procedures in the MSCF student handbook.
* If you earn a *C+* or lower, and have at least a 70% average on the homework, you may take the makeup final.
  Your performance on the makeup final can not reduce your grade, and can only increase your grade in the course by at most a full letter, up to a maximum of *B--*.
* **Regrading Policy:**
    * Your graded exams will be with your TA for Pittsburgh students, and in the MSCF office for NY students.
      You may view them in the respective office.
      However if you take them out of the office you may not request regrading of any problems.
    * If you believe a particular question has been graded incorrectly, then you must do so in writing by leaving a post-it note on the front of the exam indicating which question you want re-graded.
      *Please do **NOT** include any explanation or message.*
      Your grade will be solely based on our interpretation of what is written on the exam, and not on any explanation you provide outside the exam.
    * We will cover up the original grade, and independently regrade the requested question.
      The new grade will replace your old grade, <span class='text-danger'>**even if it is lower**</span>.
      I strongly recommend you read and understand the solutions posted online before requesting a regrade, because **your grade could become lower**.

### Grading

* Homework will count as 10% of your grade.
* The midterm will count as 30%.
* The final will count as 60%.
* In the event you miss the midterm, I will count the final as 90%, and assess a *full letter grade penalty*.

### Accommodations for Students with Disabilities

If you have a disability and have an accommodations letter from the Disability Resources office, I encourage you to discuss your accommodations and needs with me as early in the semester as possible. I will work with you to ensure that accommodations are provided as appropriate. If you suspect that you may have a disability and would benefit from accommodations but are not yet registered with the Office of Disability Resources, I encourage you to contact them at <access@andrew.cmu.edu>.

### Student Wellness

As a student, you may experience a range of challenges that can interfere with learning, such as strained relationships, increased anxiety, substance use, feeling down, difficulty concentrating and/or lack of motivation. These mental health concerns or stressful events may diminish your academic performance and/or reduce your ability to participate in daily activities. CMU services are available, and treatment does work. You can learn more about confidential mental health services available on campus [here](http://www.cmu.edu/counseling). Support is always available (24/7) from Counseling and Psychological Services: 412-268-2922.


### Faculty Course Evaluations

At the end of the semester, you will be asked to fill out faculty course evaluations.
Please fill these in promptly, I value your feedback.
As incentive, if over 75% of you have filled out evaluations on the last day of class, then I will release your grades as soon as they are available.
If not, I will release your grades at the very end of the grading period.
