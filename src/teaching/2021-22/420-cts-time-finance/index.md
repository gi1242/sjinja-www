## Course Description

The main focus of this course is to learn how to price securities in continuous time markets.
In order to do this, we will study several tools from stochastic calculus including conditional expectation, continuous time martingales, Brownian motion, Itô integrals and Itô's formula, exponential martingales, and the Girsanov theorem.
We will cover the Black-Scholes option pricing model in detail, the fundamental theorems of asset pricing, and various other securities (such as Asian options, barrier options, etc.).

### Learning Objectives

* Develop an understanding of and familiarity with stochastic calculus, the mathematical tools used to price derivative securities in continuous time.
* Use these tools to price securities in continuous time markets.


### Pre-requisites

* 21-370: Discrete time finance
* 21:325: Probability
* Differential equations (e.g. 21-260)
* The ability to read and write proofs.

### References

There is no textbook for this class.
Here are a few references; you're free to follow any one that resonates with your style.

* *Stochastic Calculus for Finance II: Continuous-Time Models by Steve Shreve.*
* I regularly teach [[../944-scalc-finance1|46-944]], a masters level course on this topic, and many of the references in that course may be of interest to you:
    - [[../944-scalc-finance1/pdfs/notes.pdf|Brief lecture notes]] I wrote.
      (Also available as a [[../944-scalc-finance1/pdfs/notes-tablet.pdf|tablet version]] for easy viewing on computers / tablets.)
    - Compactified [[../944-scalc-finance1/pdfs/scalc-compressed.pdf|slides from class]]
    - Annotated [[../944-scalc-finance1/pdfs/scalc-ann.pdf|slides from class]]
    - [[../944-scalc-finance1|46-944]] Website

## Class Policies

### Lectures

* If you must sleep, <span class='text-danger'>don't snore!</span>
* Be courteous when you use mobile devices  
* Some lectures and office hours will be conducted via [[auth/zoom.md|Zoom]] at the scheduled times.
* Please treat the Zoom lectures just as you would a regular class. Sometimes you only see one person on the screen, it's easy to forget that there are actually many participants online that you don't see. Be mindful of all the other participants.
* Keep your mic muted when you are not speaking.
* **Please enable video.** It helps me pace my lecture, and stops me from going too fast.
* If you have a question, please unmute yourself and ask, just like you would in a regular lecture. You can also use the chat / raise hand feature in Zoom.
* I will record all lectures and make the recordings available (CMU only). To join the Zoom lectures live, you will have to consent to be recorded.
* **Net Failure Policy:** If my internet dies, it might take me a few minutes to bring it back online. Please bear with me. If I can't get it fixed, you will hear from me by email regarding plans on finishing the lecture.

### Homework

* All homework must be scanned and turned in via [[auth/gradescope.md|Gradescope]].
* Please take good quality scans; homework that's too hard to read won't be graded. I recommend using a good scanning app that adjusts the contrast of your images for readability. (I use Adobe Scan myself).
* **Late homework policy:**
    - Any homework turned within the first hour of the deadline will be assessed a 20% penalty. (Note, it takes a few minutes to upload your homework/exam via [[auth/gradescope.md|Gradescope]]. So don't cut it too close to the deadline.)
    - Any homework turned after the first hour past the deadline will be assessed a 100% penalty (i.e. you won't receive credit for this homework, but if practical, your homework may still be graded).
    - To account for unusual circumstances, you may turn in two assignments up to 24 hours late without penalty, and you may also skip up to two homework assignments.
      That is, I will not assess a late penalty on the first two assignments you turn in at most 24 hours late, and I will drop the bottom two scores on your homework from your grade.
    - I will only consider making an exception to the above late homework policy if you have documented personal emergencies lasting at least **18 days**.
* I recommend starting the homework early.
  Most students will not be able to do the homework in one evening.
* You may collaborate, use books and whatever resources you can find online in order to do the homework.
  However, you **must** write your solution up independently, **and** you must fully understand any solution you turn in.
  Turning in solutions you don't understand will be treated as a violation of academic integrity.
* In order to ensure academic integrity is maintained, I will call on some subset of students to explain their solutions to me outside class.
* New material will be developed through homework problems.
  If you are unable to solve a particular problem, be sure to ask me or your TAs about it, or look up the solutions after they are posted.
* Some homework problems will also appear on your exams *with a devious twist*.
  A through understanding of the solutions (even if you didn't come up with it yourself) will invariably help you.
  But knowledge of the solution without understanding will almost never help you.
* Nearly perfect student solutions may be scanned and hosted here, with your identifying information removed.
  If you don't want any part of your solutions used, please make a note of it in the margin of your assignment.

### Exams

{#
* All exams are open book. You may use your notes, references or any online resources available.
* You may not, however, take assistance from other persons. This includes via email/messaging or posting on online discussion boards.
* You may take the exam at any time on the exam day (24 hours). You must not discuss the exam with anyone (even people outside the class) until the exam day ends.
* All exams are self proctored. You must record yourself using Zoom (audio, video, and screen), for the entire duration of the exam.
    - Start a new Zoom meeting. Share your screen (entire screen, not just a window). Record it to the cloud.
    - Be sure that the recording also shows you, either picture in picture, or separately. There are many settings in Zoom that change this, so please test it before the exam.
    - Your recording should start with you identifying yourself, and show you downloading the exam.
    - The screen recording should capture the entire screen, and can't have any time gaps; it should end with you uploading your exam on [[auth/gradescope.md|Gradescope]].
    - You should stay within your webcams field of view for the entire exam.
    - If any other electronic device is used (tablets / phones), you must also take a screen recording of that device via Zoom for the entire duration of the exam. (The only exception to this is if you *only* use a device to scan and submit your exam, and the device is *only* used at the very end where it is clear you are only submitting your exam, then I do not require a screen recording of this device.)
    - Your recording must remain accessible to me the Zoom server for at least 6 months.
    - Additionally, you must download a copy of the recording, and submit it as instructed before the exam. Exams turned in without an accompanying recording will receive no credit.
#}
* All exams are closed book, in class.
* No calculators, computational aids, or internet enabled devices are allowed.
* The final time will be announced by the registrar
  [here](https://www.cmu.edu/hub/courses/exams/index.html).
  <span class='text-danger'>
  Be aware of their schedule before making your travel plans.
  </span>

### Academic Integrity

* All students are expected to follow the academic integrity standards outlined [here](https://www.cmu.edu/policies/student-and-student-life/academic-integrity.html).
* There will be zero tolerance for academic integrity violations, and any violation will result in an automatic `R`.
  Examples of academic integrity violations include (but are not limited to):
    - Not writing up solutions independently and/or plagiarizing solutions.
    - Turning in solutions you do not understand.
    - Receiving assistance from another person during an exam.
    - Providing assistance to another person taking an exam.
* All academic integrity violations will further be reported to the university, and the university may chose to impose an additional penalty.

### Grading

* Homework will count as 30% of your grade.
* Each midterm will count as 20%.
* The final will count as 30%.

### Accommodations for Students with Disabilities

If you have a disability and have an accommodations letter from the Disability Resources office, I encourage you to discuss your accommodations and needs with me as early in the semester as possible. I will work with you to ensure that accommodations are provided as appropriate. If you suspect that you may have a disability and would benefit from accommodations but are not yet registered with the Office of Disability Resources, I encourage you to contact them at <access@andrew.cmu.edu>.

### Student Wellness

As a student, you may experience a range of challenges that can interfere with learning, such as strained relationships, increased anxiety, substance use, feeling down, difficulty concentrating and/or lack of motivation. These mental health concerns or stressful events may diminish your academic performance and/or reduce your ability to participate in daily activities. CMU services are available, and treatment does work. You can learn more about confidential mental health services available on campus [here](http://www.cmu.edu/counseling). Support is always available (24/7) from Counseling and Psychological Services: 412-268-2922.


### Faculty Course Evaluations

At the end of the semester, you will be asked to fill out faculty course evaluations.
Please fill these in promptly, I value your feedback.
As incentive, if over 75% of you have filled out evaluations on the last day of class, then I will release your grades as soon as they are available.
If not, I will release your grades at the very end of the grading period.
