title: Videos of Lectures, and Notes

These are PDFs of everything I wrote on the screen during Zoom lectures, along with links to videos for every lecture that the recording worked.
The materials from office hours from your TA's are [[auth/gdrive.md|here]].

{% set videos = dict(
  l01='https://cmu.zoom.us/rec/share/F789fIkbADz1VUIR9wdcflAemNc8sVsGvhoM88D82mCYOvLwcP3hOd7PVtGai-KT.jZC2hssDZA6DQxid',
  l02='https://cmu.zoom.us/rec/share/_VqKVDoYR5mkDKJ9BCP3m1Vi-wTkDc5FxXejdJvFFltWAxmZ3Gooz-GOhfahbt_v.s4xEcWVUtpeUjwr7',
  l03='https://cmu.zoom.us/rec/share/yd8GEaZovV2b_ReJXUlcOGoIGRxGhoC1UBa-GKbuN6DRTa3hdduMsCgSWu1XnZHf.5tdl4IlYcnj0PGPE',
  l04='https://cmu.zoom.us/rec/share/4KZq1w-p3no_c75eFQe74XjVrSMpO2ULupK9bLlBfOHuCIh5X5BVm7y2vVuqCxwc.wp5G8Ufzz8YSTZ2H',
  l05='https://cmu.zoom.us/rec/share/iD00nJGNMHoVzJpsGLGd-8vD2C_w4Fab7K8QL9NnJA3iLYQd7O5XGg2TxToIeks.-oH1nw6bP_5V10qO',
  l08='https://cmu.zoom.us/rec/share/JsJpszw5INiBljdKGIXQEKYOuT2NtItOnd7HtTDYW_TMFw4T-I_4u914ILcm3CyQ.Ebjjqon86mMVxnvn',

  o99='https://cmu.zoom.us/rec/share/dfP7p3PkdUJ8jefzDgzHbYvQIfowt1DT1kmFNWPgfE7wpsfRAEo7Dz1dR2QcU6u0.az2THmAgDAigOYBQ',
) -%}
{% for f in glob('pdfs/lec/20*-?[0-9]*.pdf') | sort -%}
  {% set fn = f | replace( 'pdfs/lec/', '') | replace( '.pdf', '') -%}
  {% set date = fn[0:8] -%}
  {% set type = fn[9:] -%}
  {% if loop.first -%}
    <table class='table'>
      <thead>
        <tr>
          <th scope='col'>Date</th>
          <th scope='col'>Event</th>
          <th scope='col'>PDF</th>
          <th scope='col'>Video</th>
        </tr>
      </thead>
  {% endif -%}
      <tbody>
        <tr>
          <td>{{date[0:4]}}-{{date[4:6]}}-{{date[6:8]}}</td>
          <td>
            {%- if type[0] == 'r' -%}
              Recitation
            {%- elif type[0] == 'o' -%}
              Office hour
            {%- else -%}
              Lecture
            {%- endif -%}
            {{' #' ~ type[1:]}}
          </td>
          <td><a href='{{get_link(f)}}'>🗐</a></td>
          {%- if videos[type] %}
            <td><a href='{{videos[type]}}'>🎥</a></td>
          {%- endif %}
        </tr>
  {% if loop.last -%}
      </tbody>
    </table>
  {% endif -%}
{% else -%}
    * *Videos and notes will be posted as the semester progresses.*
{% endfor %}

<div class='alert alert-warning small'>
  <b>Note:</b> If the video of the most recent lecture isn't online by 10:30pm on the day of class, then there is most likely an error I'm not aware of. Please email me.
</div>
