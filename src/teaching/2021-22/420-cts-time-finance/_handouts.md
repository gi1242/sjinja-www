{%- from 'teaching/courses.j2' import old_teaching -%}
* [[auth/zoom.md]]
<div class='my-2'></div>
* [[lectures.md]]
* Slides from Zoom lectures:
    - [[pdfs/ctf-today.pdf|Next lecture (unannotated)]]
    - [[pdfs/ctf-ann.pdf|All lectures (annotated)]]
    - [[pdfs/ctf-compressed.pdf|All lectures (compact)]]
    - [LaTeX source](https://gitlab.com/gi1242/cmu-math-420)
* [[auth/gdrive.md]]
<div class='my-2'></div>
* [[auth/2021-midterm1.pdf|2021 Midterm 1]] and [[auth/2021-midterm1-sol.pdf|solutions]].
* Your [[pdfs/midterm1.pdf|Midterm 1]] and [[auth/midterm1-sol.pdf|solutions]].
{#
* [[../../2020-21/370-dtime-finance/pdfs/midterm2.pdf|2020 Midterm 2]] and [[auth/2020-midterm2-sol.pdf|solutions]].
* Your [[pdfs/midterm2.pdf|Midterm 2]] and [[auth/midterm2-sol.pdf|solutions]].
* [[../../2020-21/370-dtime-finance/pdfs/final.pdf|2020 final]] and [[auth/2020-final-sol.pdf|solutions]].
* Your [[auth2/final.pdf|Final]] and [[auth2/final-sol.pdf|Solutions]]
-#}
{#
* [[auth/midterm2.md|Solutions to Midterm 2]].
<div class='my-2'></div>
-#}
<div class='my-2'></div>
* [[../370-dtime-finance|370 website]]
* [[../944-scalc-finance1|944 website]]
