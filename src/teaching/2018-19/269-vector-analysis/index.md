## Course Description

This course is an introduction to vector analysis, and is an honors version of 21-268.
The material covered will be a strict super-set of 268, and more emphasis will be placed on writing rigorous proofs.
The treatment of differential calculus will be through and rigorous.
In the interest of time, however, many results on integral calculus will be stated without proof, or proved under simplifying assumptions.

### Tentative Syllabus

* Functions of several variables, regions and domains, limits and continuity.
* Sequential compactness.
* Partial derivatives, linearization, Jacobian.
* Chain rule, inverse and implicit functions and geometric applications.
* Higher derivatives, Taylor's theorem, optimization, vector fields.
* Multiple integrals and change of variables, Leibnitz's rule.
* Line integrals, Green's theorem.
* Path independence and connectedness, conservative vector fields.
* Surfaces and orientability, surface integrals.
* Divergence theorem and Stokes's theorem.

### Pre-requisites

Students are expected to have a **through** knowledge of one variable calculus, linear algebra and some familiarity with writing proofs.
The official pre-requisites are *21-122* and a grade of *B* or better in *21-242*.


### Textbook and References

The material covered in this course is standard and can be found in many good references.
(Translation: I won't write notes!)
However, I will cover material in a manner that's tailored to this course, so you might not find an identical treatment in your favourite reference.
Here are a few references of varying levels of difficulty.
Choose whatever works best for you.

* [[../../2015-16/268-multid-calc/pdfs/brief-notes.pdf|Brief notes for 268]].
  <small>(In 268, less attention was paid to proofs. A lot of these gaps will be filled in in this course, and more material will be covered.)</small>
* **Introduction to Multivariable Mathematics** by Leon Simon
  <small>(This covers almost everything we will cover in this course, and is an excellent read.)</small>
* **Calculus on Manifolds** by Spivack.
* **Lecture Notes on Multivariable Calculus** by Barbara Niethammer and Andrew Dancer.
  Currently the book can be found online [here](https://courses.maths.ox.ac.uk/node/view_material/5196), but the link may change as time progresses.
    <small>(This covers the differential calculus portion of this class.)</small>
* [[../../2016-17/269-vector-analysis/pdfs/giovanni-2015.pdf|Lecture notes]] by Giovanni Leoni.
  <small>(This covers limits, continuity and the differential calculus portion of the class.)</small>
* *Introduction to Analysis* Maxwell Rosenlicht.
  <small>(Bit old, but cheap Dover book and pretty good.)</small>
* [Understanding Analysis](https://link.springer.com/book/10.1007/978-1-4939-2712-8) by Stephen Abbott.
  <small>(Good treatment of analysis and one variable differential calculus.)</small>
* *Principles of Mathematical Analysis* by W. Rudin
  <small>(Excellent "classic" and cheap.)</small>

## Class Policies

### Lectures

* If you must sleep, <span class='text-danger'>don't snore!</span>
* Be courteous when you use mobile devices.

### Homework

* Homework must be turned in **at the beginning of class** on the due date.
* Late homework will *NOT* be accepted.
  However, to account for unusual circumstances, the bottom 20% of your homework will not count towards your grade.
* I will only consider making an exception to the above late homework policy if you have documented personal emergencies lasting at least **18 days**.
* You may collaborate on the homework, however, you may only turn in solutions which you fully understand and have written up independently.
* A few homework questions will appear on your exams.
* Nearly perfect student solutions may be scanned and hosted here, with your identifying information removed. If you don't want any part of your solutions used, please make a note of it in the margin of your assignment.

### Exams

* All exams are closed book, in class.
* No calculators, computational aids, or internet enabled devices are allowed.
* The final time will be announced by the registrar
  [here](https://www.cmu.edu/hub/courses/exams/index.html).
  <span class='text-danger'>
  Be aware of their schedule before making your travel plans.
  </span>

### Grading

* Your performance on the homework, midterm and final will each be converted to a numerical grade 0 and 4.5 "using a curve".
* Your overall grade will be computed as a weighted average with your final counting for 50%, your better midterm 30% and homework 20%.
* Your final letter grade will be computed from your numerical grade using [the standard scale](https://www.cmu.edu/policies/student-and-student-life/grading.html).
