* [[schedule.md]]
* [[videos.md]]
* [[../../2016-17/269-vector-analysis/pdfs/2012-midterm1.pdf|Midterm 1 (2012)]]
  (and [[auth/2012-midterm1-sol.pdf|solutions]])
* [[../../2016-17/269-vector-analysis/pdfs/2012-midterm2.pdf|Midterm 2 (2012)]]
  (and [[auth/2012-midterm2-sol.pdf|solutions]])
* [[../../2016-17/269-vector-analysis/pdfs/midterm1.pdf|Midterm 1 (2016)]]
  (and [[auth/2016-midterm1-sol.pdf|solutions]])
* Your [[pdfs/midterm1.pdf|midterm 1]] (and [[auth/midterm1-sol.pdf|solutions]]).
* [[../../2016-17/269-vector-analysis/pdfs/midterm2.pdf|Midterm 2 (2016)]]
  (and [[auth/2016-midterm2-sol.pdf|solutions]])
* Your [[pdfs/midterm2.pdf|midterm 2]] (and [[auth/midterm2-sol.pdf|solutions]]).
* [[pdfs/lagrange.pdf|Lagrange multipliers]]
* [[../../2016-17/269-vector-analysis/pdfs/final.pdf|Final (2016)]]
  (and [[auth/2016-final-sol.pdf|solutions]])
{#-
* [[pdfs/exam-questions.pdf|A few "exam like" questions]]
#}
* Your [[pdfs/final.pdf|final]] (and [[auth/final-sol.pdf|solutions]]).
* [[../../2017-18/268-multid-calc|268 website (2018)]]
* [[../../2016-17/269-vector-analysis|269 website (2017)]]
