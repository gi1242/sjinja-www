title: Video Lectures

Here are videos of lectures I did online (on Zoom) on days when class could not be held.
In addition to the video, you can download the PDF of what I wrote on the screen.

1. **Jan 30: Continuity of Inverses**:
    [[videos/20190130-continuity-of-inverses.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20190130-continuity-of-inverses.png'>
      <source src='videos/20190130-continuity-of-inverses.mp4' type='video/mp4'>
    </video>

2. **Feb 22: Inverse Function Theorem (part 1)**:
    [[videos/20190222-inverse-fn1.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20190222-inverse-fn1.png'>
      <source src='videos/20190222-inverse-fn1.mp4' type='video/mp4'>
    </video>

3. **Feb 23: Inverse Function Theorem (part 2)**:
    [[videos/20190223-inverse-fn2.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20190223-inverse-fn2.png'>
      <source src='videos/20190223-inverse-fn2.mp4' type='video/mp4'>
    </video>

4. **Apr 16: Greens theorem**:
    [[videos/20190416-greens-theorem.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20190416-greens-theorem.png'>
      <source src='videos/20190416-greens-theorem.mp4' type='video/mp4'>
    </video>

5. **May 1: Two IOU's in the proof of the Divergence Theorem**:
    [[videos/20190501-div-thm-iou.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20190501-div-thm-iou.png'>
      <source src='videos/20190501-div-thm-iou.mp4' type='video/mp4'>
    </video>
