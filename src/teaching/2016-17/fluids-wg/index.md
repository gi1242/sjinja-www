title: Working Group: Mathematical Fluid Dynamics
subtitle: Fall 2016

## Logistical Information

<table class='table'>
  <tr>
    <th scope='row'>Organizers</th>
    <td>Gautam Iyer and Ian Tice</td>
  </tr>
  <tr>
    <th scope='row'>Location</th>
    <td>Usually Tuesdays 2:30PM in WEH 7218.</td>
  </tr>
  <tr>
    <th scope='row'>Current Schedule</th>
    <td><a href='http://www.math.cmu.edu/CNA/cnawgroups.php'>www.math/CNA/cnawgroups.php</a></td>
  </tr>
  <tr>
    <th scope='row'>Mailing List</th>
    <td><a href='https://lists.andrew.cmu.edu/mailman/listinfo/fluids-wg'>lists.andrew/mailman/listinfo/fluids-wg</a></td>
  </tr>
</table>

## Topics and References

The topics and speakers are as follows.

* **Jack Schaeffer (9/20):** Kinetic Theory

* **Lei Wu (9/27, 10/4):** Hydrodynamic Limit (2 talks)
  (Reference: [Miller](http://www.damtp.cam.ac.uk/user/ps57/MATH557Notes.pdf))

* **Josh Ballew (10/11):**
  From a Mesoscopic to Macroscopic Description of Fluid-Particle Interaction.
  (References:
    [Carrillo, Goudon](http://www.tandfonline.com/doi/abs/10.1080/03605300500394389),
    [Millet, Vasseur](http://www.worldscientific.com/doi/abs/10.1142/S0218202507002194),
    [Millet, Vasseur](http://link.springer.com/article/10.1007/s00220-008-0523-4))

* **Jian-Guo Liu (10/18)** Curve evolution equations related to the Camassa-Holm equation "$B$-equation".
  (Reference: [Duan Liu](https://services.math.duke.edu/~jliu/research/pdf/Duan_Liu_DCDS_A_2014.pdf))

* **Bob Pego (10/25):** Euler Sprays (References: [Pego, Slepčev, Liu](http://arxiv.org/abs/1604.03387))

* **Yue Pu (11/1):** Minimal geodesics on groups of volume-preserving maps and generalized solutions of the Euler equations
  (References: [Brenier](http://onlinelibrary.wiley.com/doi/10.1002/%28SICI%291097-0312%28199904%2952%3A4%253C411%3A%3AAID-CPA1%253E3.0.CO%3B2-3/abstract))

* **Xiao Xu (11/8):** Vorticity growth in the Euler Equations.
  (References: [Kiselev Šverák](http://annals.math.princeton.edu/2014/180-3/p09))

* **Antoine Remond-Tiedrez (11/15):** Helmholtz decompositions for the Stokes problem (Reference: [Boyer Fabrie](http://link.springer.com/book/10.1007%2F978-1-4614-5975-0))

* **Noel Walkington (11/22):** Properties of the Equations Modelling Viscoelastic Fluids
  (References:
    [Perroti, Wang, Walkington](http://www.math.cmu.edu/%7Enoelw/Noelw/Papers/oldroyd.pdf),
    [Walkington](http://www.math.cmu.edu/%7Enoelw/Noelw/Papers/Wa11m2an.pdf))

* **Giovanni Gravina (11/29):** A variational approach for water waves
  (References:
    [Arama Leoni](http://cvgmt.sns.it/media/doc/paper/2439/arama-leoni-2012-01-23.pdf),
    [Alt Caffarelli](https://eudml.org/doc/152360#content))

* **Laurent Dietrich (12/6):** Speed-up of combustion fronts in shear flows.
  (References:
    [Hamel, Zlatoš](http://link.springer.com/article/10.1007/s00208-012-0877-y))
