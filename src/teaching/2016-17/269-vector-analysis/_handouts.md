* [[schedule.md]]
* [[../../2015-16/268-multid-calc|268 website (2015)]]
* [269 website (2012)](https://www.math.cmu.edu/~gautam/teaching/2011-12/269-vector-analysis/)
* [[pdfs/2012-midterm1.pdf|Midterm 1 (2012)]] (and [[auth/2012-midterm1-sol.pdf|solutions ]])
* [[pdfs/2012-midterm2.pdf|Midterm 2 (2012)]] (and [[auth/2012-midterm2-sol.pdf|solutions ]])
* Your [[pdfs/midterm1.pdf|midterm 1]] (and [[auth/midterm1-sol.pdf|solutions]]).
* [[pdfs/exam-questions.pdf|A few "exam like" questions]]
* Your [[pdfs/midterm2.pdf|midterm 2]] (and [[auth/midterm2-sol.pdf|solutions]]).
* [[pdfs/lagrange.pdf|Lagrange multipliers]]
* [[videos.md]]
* Your [[pdfs/final.pdf|final]] (and [[auth/final-sol.pdf|solutions]]).
