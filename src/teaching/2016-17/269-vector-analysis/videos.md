title: Screencasts

Here are screencasts of material for the lecture I was away for (Apr 19th, 2017).
Also, since I might be a bit rushed when I talk about the divergence theorem (on May 1, 3) I made a screen cast showing parametrization invariance of surface integrals instead of doing it in class.

In addition to the video, you can download the PDF of what I wrote on the screen.
Also, most of the material covered here can be found in my typed notes on
[[../../2015-16/268-multid-calc/pdfs/ch5-lint.pdf|line]]
and 
[[../../2015-16/268-multid-calc/pdfs/ch6-sint.pdf|surface]]
integrals.

1. Conservative / potential forces.
    [[videos/20170418-conservative-forces.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20170418-conservative-forces.png'>
      <source src='videos/20170418-conservative-forces.webm' type='video/webm'>
      <source src='videos/20170418-conservative-forces.mp4' type='video/mp4'>
    </video>

2. Greens theorem and proof.
    [[videos/20170418-greens-thm.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20170418-greens-thm.png'>
      <source src='videos/20170418-greens-thm.webm' type='video/webm'>
      <source src='videos/20170418-greens-thm.mp4' type='video/mp4'>
    </video>

3. Parametrization invariance of surface integrals.
    [[videos/20170430-param-inv.pdf|PDF of screen writing]]

    <video controls style='width: 100%'
        poster='videos/20170430-param-inv.png'>
      <source src='videos/20170430-param-inv.webm' type='video/webm'>
      <source src='videos/20170430-param-inv.mp4' type='video/mp4'>
    </video>
