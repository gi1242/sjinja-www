title: Lecture Schedule

Here is a list of topics that are covered in class, in the order they were covered.
All theorems on differentiation (including inverse and implicit function theorems) will be proved rigorously.
In the interest of time, however, a few proofs on integration will be skipped.

## Limits

* Distance in $\R^d$.
* $\varepsilon$-$\delta$ definition of limits
* Uniqueness, sums and products.
* Continuity. Continuity of composites.
* Open sets and domains.

## A crash course in Analysis

* Sequences
* Infimum and Supremum 
* Bolzano-Weierstrass theorem
* Proof of the extreme value theorem
* Continuity of inverses
* Uniform continuity

## A crash course in one variable differential calculus

* Derivatives
* Mean value theorems

## Differentiation in higher dimensions

* Partial and directional derivatives
* The derivative as a linear transformation
* Continuity of partials
* The chain rule
* Mean value theorem
* Higher order partials and Clairut's theorem.
* Taylor's theorem
* Maxima and minima

## Inverse and Implicit functions

* Proof of the inverse function theorem
* Coordinate changes
* The implicit function theorem
* Parametric curves.
* Surfaces, Manifolds, and tangent spaces.
* Lagrange multipliers

## Multiple Integrals

* Double and triple integrals
* Iterated integrals and Fubini's theorem
* Change of variables

## Line integrals

* Line integrals
* Parametrization invariance and arc length
* The fundamental theorem
* Greens theorem

## Surface integrals

* Surface integrals
* Surface integrals of vector functions
* Stokes theorem
* Conservative and potential forces
* Divergence theorem
