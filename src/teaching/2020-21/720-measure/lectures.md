title: Videos of Lectures, and Notes

These are PDFs of everything I wrote on the screen during lectures, along with links to videos for every lecture that the recording worked.
(Note: Many office hours by me/your TAs have notes on the shared Google Drive folder instead of here.)

{% set videos = dict(

  l01='https://cmu.zoom.us/rec/share/5JFPJa7J7EpJYdLO1mL7YbxmNKXPX6a8gSIW_fFfyssb_iI8xDHulszcWsy8jUU',
  l02='https://cmu.zoom.us/rec/share/TUGv0XdOXYV7opi7klk_Lu58Gsv-ujn5zbiA80yInYVSRcb9bf_J6T9tNm0y4IaX.9w6tVdQuA8RFWDxG',
  l03='https://cmu.zoom.us/rec/share/xhTNm97DBjqMojmqOAba9Rs38ixTo5P_VBFDoVefg_2kIiM8i_QxLD6m_37c-YKL.p6S64U_RtWbvlnSF',
  l05='https://cmu.zoom.us/rec/share/O88-AqyKnn_vZp-j9DeFMzhpBcYYiSal5ecP8_w7cqTT5wqGCCKe8yrWRflK4snq.OgPY66CTqwRbEiF5',
  l06='https://cmu.zoom.us/rec/share/_n23s6rGuiGzo3RvhcJktZLTjZb7A1gnyeDD7Mr4elGZnDNlCUOsuqQmw0PNZgfy.Y_fpB9mAcYZn3Unj',
  l07='https://cmu.zoom.us/rec/share/MViTEiBYuq-Aa3TQxag2_0g-DCczOhHvee8kdazQ5lzReBpnnqqjp26p6S0Vmt7a.MwyP44_oQjFsmYYH',
  l08='https://cmu.zoom.us/rec/share/e1kBbrbxZ-CxLEEesjU1-X-SAeoI2EcLhqLGzNSgPKhAvDYFAh2AxpDziktBZIM9.ub3xeDRrb0zkXn2M',
  l09='https://cmu.zoom.us/rec/share/jjE4tu9nVY9H8v_H95xWHocmG_pX1aj2nDILA6gP8L57hKuqTKKoJKVjQKlLCqu6.ZKhZDCG2iUwlsO5m',
  l10='https://cmu.zoom.us/rec/share/dGE8KpnNDsnr7eGvf0zPsTdO6_DDr7URiLudJS52lEFPRTFov84mFoI8Rm2stJid.ffJ7SLzU2riSCxG6',
  l11='https://cmu.zoom.us/rec/share/yYYdzJlLw-KRtsbskboRJIKRjJFdYA3YXqfN-oF-BKvXgc6sG7ebzggR96BmHShg.OSwNgX6NoIAneR5p',
  l12='https://cmu.zoom.us/rec/share/i0bx55181oEv4Ujt6eF6nPm9QuZx_0v5NsZlsQsHXoVRAe_UDSWjVIn_WErxa8OT.CaL-13NVHcI3-b5F',
  l13='https://cmu.zoom.us/rec/share/xQR8Qk2l9JvwaFuu6psAh9vATWnOqvYRFp1FgG2sBhoILbKdbl_fId-0TDIgbG2_.a2UynOmVnZ0GbrjC',
  l14='https://cmu.zoom.us/rec/share/cqbIVPhkaEfV6YcuXnhtV-b_nyeFk9---wBzgUkBFpp8Hcwsi0TaZro4tc527ugu.Kou3OJMdxGQ6LaFb',
  l15='https://cmu.zoom.us/rec/share/A4z93cTxZDbaFWXpEaZtCfUfYk3DmUp4BSbRfi_kM94Hwyubl5spp1NUoChJIK6b.NQ7BOd7MWVGXCUda',
  l16='https://cmu.zoom.us/rec/share/BzfcxDMGKfs44No-OK_YjBexEck-4uLNoaMI62kJfF0R9BNiDiGT0GbGh-r_u4dR.ngzgybUtnau9irhi',
  l17='https://cmu.zoom.us/rec/share/CwBriJCfQG-EsdqTtnt9Bk2gRTwcXzX1TkhTeK-8GDvMOrSzgvdOC7fD7DUZhANZ.0JhpiJTZbOD2MrJh',
  l18='https://cmu.zoom.us/rec/share/qYuQKV7qMUo5LDRz-6HMyrq-J5FhGpzWxGMbRIZ73UzSEBwO-K35jB-6PBowcvd6.6GEVyFbxomS6OtZT',
  l19='https://cmu.zoom.us/rec/share/caEy3TNHfY58zrzwNjTFHOsP8vhCNmMvYtV38xojd6p0DHSrXAlT3IEPcxoiNURt.C5IBbfQ6KNx_bLa4',
  l20='https://cmu.zoom.us/rec/share/P-F9bQc5C5tqwZTW28SHqm-RC3wTeYX8qN9zWFrAPhrcrxhJK6gvjEoa7hUPXLnS.re2nXu0WdWXksoS9',
  l21='https://cmu.zoom.us/rec/share/qbxtk_ecWw3ODvKw_JN_8Se9eX1aokTC3JA3qIYYqoOu-DtVyuJ8jxwOK69fv584.gCCQ8M7p-IPJ1jPa',
  l22='https://cmu.zoom.us/rec/share/xSbHKUmE3Bhia7lKtUUvS5WWvNyfmcZPUhfX4HZdsWeysz5ydCdOfe35J8-2b3sM.pm3cKxEkgepnsFzT',
  l23='https://cmu.zoom.us/rec/share/qbbKGYo-IpYxgBQhXU9EDlebhFWpIlQkUUVJyQq_ClVj4fqg2JXpIE74SLz9qVI9.lMgQDaxQTDlxIa5P',
  l24='https://cmu.zoom.us/rec/share/fUkNL9qMJl9MIMokUTCAuHKl04omtP_m1Lc6Q4NonN2vOb3ZB1bOotbLiuHMTh57.cnZm6f_UoE7cW_x4',
  l25='https://cmu.zoom.us/rec/share/eHl79R-67IEB7qBtVbIBPlb8D7SKawRvIokFEc5HuKzfe0_FfEP_F-FEBLbP3V4R.rDTylmkdkXxd_p2D',
  l26='https://cmu.zoom.us/rec/share/HA9eVFhZ6u6jmjmH_TgGDssK_l-EJLWlFcQ3VYZZ4mTg8ZYmxIWMBxSf9svsQXma.cRsi_FjgA92u4-dp',
  l27='https://cmu.zoom.us/rec/share/YDwg4A-7tIzUySOuL8WTVJ9ypsj9DsMipDgC68w1n_O6KZXskwYnJ7OScXv5wOYm.OIXp5wFVQ1Xh7weH',
  l28='https://cmu.zoom.us/rec/share/48U2BwEw8R0OQo9Ca2jJBnDLDcXePCJvrnK2kDsIP0YeP2Ezr2Mfj4Ok0bHod5g.vmLfHCy8Lj__tS7N',
  l29='https://cmu.zoom.us/rec/share/qQEwIOrTAHmnWSov4WCurk7CBrqAZOWKFfoyxuvX1WbzQoeVOgtqEO7gKKzxEC7Z.X9oMBclJ1XiszgSl',
  l30='https://cmu.zoom.us/rec/share/LrCiK-cKp-xqkZQ9VZhGn2yaYoB_lYusgzeQt2OsKaY7THu0nLsvs0750HyBMwi5.gzJEWgF0t6CXgj7h',
  l31='https://cmu.zoom.us/rec/share/MDZXm7gsGWKrCwrsApbY6EV5dLG7-AdUX5roTHMoUF5P55IVTEfMlxiAMObMX-Wr.3UKHeqvO0tPKe8yo',
  l32='https://cmu.zoom.us/rec/share/Exysl4gs4vjTLBTztl12W_0FbpYbTJPXNiQn6UlPRPkzYWd7lzmq3qRQ2EfAWXSY.SYL0Hjk6Cex_pDM5',
  l33='https://cmu.zoom.us/rec/share/doQyvvywFAmmEb_cisNhzA7gLIshiLpZeS8m680O7lZhZx0H1J5jsLJPPKrcCGqS.oLllS0kKYLamzz5J',
  l34='https://cmu.zoom.us/rec/share/VmfXg2ws2Zk745VLT8uErwdW9Mdqzp658QkdI0V8q69YGxxut6s-nduNetdqylE.TrcmEQY4_L09ZRF6',
  l35='https://cmu.zoom.us/rec/share/B90hdXycf5qoehSZl4semaowg9D-TW9pvMeH4u4HV8H4tysAGUQXL6FUD_wXRqCM.2KrIlXek6W8KIcxF',
  l36='https://cmu.zoom.us/rec/share/MTy-LgMrCgYLQEOK1fKKoRU5sv8IVtlQQea1hfTMn4kgVmk9mPcwayxrVjt7_N0.4N0UqrrzRCJy3_l4',
  l37='https://cmu.zoom.us/rec/share/mkih355nNTo3I1sFsGjQipIZny7MZuVz7MIWM533Zd-EnZxwupd13Pv-rdS5L4jg.NF63X9ihOVqLviNF',
  l38='https://cmu.zoom.us/rec/share/F5-Z0uXzKtMLlPWbBnroXepYxPRsjqGsemksal0EN51MPa8L-HLXzrrJg86wNOkS.azj48n_haFgQtJpr',

  o06='https://cmu.zoom.us/rec/share/QgWZECIicYQta22a7gBNlPGOq8btGQnx3p77RCFQiz3gTpEG5M5p0x_I3fvUs2Bb.sVAN0jJ16IxEIMnx',

) -%}
{% for f in glob('pdfs/lec/20*-?[0-9]*.pdf') | sort -%}
  {% set fn = f | replace( 'pdfs/lec/', '') | replace( '.pdf', '') -%}
  {% set date = fn[0:8] -%}
  {% set type = fn[9:] -%}
  {% if loop.first -%}
    <table class='table'>
      <thead>
        <tr>
          <th scope='col'>Date</th>
          <th scope='col'>Event</th>
          <th scope='col'>PDF</th>
          <th scope='col'>Video</th>
        </tr>
      </thead>
  {% endif -%}
      <tbody>
        <tr>
          <td>{{date[0:4]}}-{{date[4:6]}}-{{date[6:8]}}</td>
          <td>
            {%- if type[0] == 'r' -%}
              Recitation
            {%- elif type[0] == 'o' -%}
              Office hour
            {%- else -%}
              Lecture
            {%- endif -%}
            {{' #' ~ type[1:]}}
          </td>
          <td><a href='{{get_link(f)}}'>🗐</a></td>
          {%- if videos[type] %}
            <td><a href='{{videos[type]}}'>🎥</a></td>
          {%- endif %}
        </tr>
  {% if loop.last -%}
      </tbody>
    </table>
  {% endif -%}
{% else -%}
    * *Notes will be posted as the semester progresses.*
{% endfor %}
