{%- from 'teaching/courses.j2' import old_teaching -%}
* [[auth/zoom.md]]
* [[lectures.md]]
<div class='my-2'></div>
* [[pdfs/measure-ann.pdf|Annotated Slides from class]]
* [[pdfs/measure.pdf|Compactified un-annotated slides from class]]
* Student lecture notes
    - [Eugene Choi (2013/14)]({{old_teaching}}/2013-14/720-measure/pdfs/eugenes-notes.pdf)
    - [Adam Gutter (2014/15)]({{old_teaching}}/2014-15/720-measure/pdfs/adams-notes.pdf)
* [The LaTeX source for all notes](https://gitlab.com/gi1242/cmu-math-720)
<div class='my-2'></div>
* [A more general Change of Variables](https://www.jstor.org/stable/2317484?seq=1)
* [[pdfs/ergodic.pdf|The ergodic theorem and continued fractions]]
<div class='my-2'></div>
* 2013 [[pdfs/2013-midterm.pdf|Midterm]] and [[auth/2013-midterm-sol.pdf|Solutions]]
* 2014 [[pdfs/2014-midterm.pdf|Midterm]] and [[auth/2014-midterm-sol.pdf|Solutions]]
* Your [[pdfs/midterm.pdf|Midterm]] and [[auth/midterm-sol.pdf|Solutions]]
* 2013 [[pdfs/2013-final.pdf|Final]] and [[auth/2013-final-sol.pdf|Solutions]]
* 2014 [[pdfs/2014-final.pdf|Final]] and [[auth/2014-final-sol.pdf|Solutions]]
* Your [[pdfs/final.pdf|Final]] and [[auth/final-sol.pdf|Solutions]]
{#
* [[auth/midterm.md|Your midterm and solutions]]
-#}
<div class='my-2'></div>
* [720 website from 2014]({{old_teaching}}/2014-15/720-measure)
