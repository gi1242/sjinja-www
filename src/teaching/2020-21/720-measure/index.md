## Course Description

This is a first graduate course on Measure Theory, and will cover the basics of measures, Lebesgue integration, differentiation, product measures and $L^p$ spaces.
Time permitting we will also introduce the basics of Fourier analysis.

### Learning Objectives

* Develop familiarity with measures, Lebesgue integration, differentiation and convergence.
* Get accustomed to the level and difficulty of math graduate courses.

### Pre-requisites

* A solid undergraduate real analysis course

### Tentative Syllabus

* Outer measures, measures, $\sigma$-algebras, Carathéodory's extension theorem. Borel measures, Lebesgue measures.
* Measurable functions, Lebesgue integral (Monotone Convergence Theorem, Fatou's Lemma, Dominated Convergence Theorem).
* Modes of Convergence (Egoroff's Theorem, Lusin's Theorem)
* Product Measures (Fubini-Tonelli Theorems), $n$-dimensional Lebesgue integral
* Signed Measures (Hahn Decomposition, Jordan Decomposition, Radon-Nikodym Theorem, change of variables)
* Differentiation (Lebesgue Differentiation Theorem)
* $L^p$ Spaces, Hölder's inequality, Minkowskii's inequality, completeness, uniform integrability, Vitali's convergence theorem.
* This will be followed by some special topics (e.g. Fourier Analysis). 

### Course Outline

* The course will start by constructing the Lebesgue measure on $\mathbb{R}^n$, roughly following Bartle, chapters 11--16.
* After this, we will develop integration on abstract measure spaces roughly roughly following Cohn, chapters 1--6 or Folland.
* If time permits, I will continue with some Fourier Analysis roughly following Folland chapter 8.

### References

Since measure theory is fundamental to modern analysis, there is no dearth of references (translation: I'm not writing lecture notes).
I'm listing a few good references here.
If you don't know which one to choose, I'd suggest trying either Cohn or Folland.

* *Real Analysis by G. B. Folland.* (A through, modern treatment with a few nice additional topics (topology, functional analysis, Fourier analysis and probability). Strongly recommended if you're going to do your Ph.D. in something Analysis related.)
* *Measure theory by D. L. Cohn.* (A bit easier to read, and more focussed than [Folland])
* *Real and Complex Analysis by W. Rudin.* (A classic. Excellent, except for the construction of Lebesgue measure.)
* *Lebesgue integration on Euclidean space by F. Jones.* (A bit verbose, and easy to read, but at a level a little lower than this course.)
* *Real analysis by H. L. Royden.* (Again a great read, but a at a level a little lower than this course.)
* If you prefer learning from lecture notes, here are some by [Lenya Ryzhik](http://math.stanford.edu/~ryzhik/STANFORD/205-STANF/notes-205.pdf) and [Terry Tao](http://terrytao.wordpress.com/books/an-introduction-to-measure-theory/). The last one is available as a PDF, and also as a regular published book.

Finally, when I taught this course in 2013/14, two students typed their notes up and shared them. Their notes are here:

* Lecture notes from 2013 by [Eugene Choi]({{old_teaching}}/2013-14/720-measure/pdfs/eugenes-notes.pdf)
* Lecture notes from 2014 by [Adam Gutter]({{old_teaching}}/2014-15/720-measure/pdfs/adams-notes.pdf)

If you'd like to copy/edit these notes, the full latex source is available [here](https://gitlab.com/gi1242/cmu-math-720), or can be cloned via git at [git.math.cmu.edu/pub/201312-measure](git://git.math.cmu.edu/pub/201312-measure).
If you edit these notes, please consider making your changes available.

## Class Policies

### Zoom Lectures

* If you must sleep, <span class='text-danger'>don't snore!</span>
* All lectures and office hours will be conducted via [[auth/zoom.md|Zoom]] at the scheduled times.
* Please treat the Zoom lectures just as you would a regular class. Sometimes you only see one person on the screen, it's easy to forget that there are actually many participants online that you don't see. Be mindful of all the other participants.
* Keep your mic muted when you are not speaking. Use headphones if you can.
* **Please enable video.** It helps me pace my lecture, and stops me from going too fast.
* If you have a question, please unmute yourself and ask, just like you would in a regular lecture. Most days I will have a second device open with a view of all participants. So if you raise your hand physically, I *may* be able to see it. In the participant list, zoom also has a "raise hand" icon. If you do that, I should see it too, and will ask for your question as soon as I pause.
* I will record all lectures and make the recordings available (CMU only). To join the Zoom lectures live, you will have to consent to be recorded.
* **Failure Policy:** If my internet dies, it might take me a few minutes to bring it back online. Please bear with me. If I can't get it fixed, you will hear from me by email regarding plans on finishing the lecture.

### Homework

* All homework must be scanned and turned in via Gradescope.
* Please take good quality scans; homework that's too hard to read won't be graded. I recommend using a good scanning app that adjusts the contrast of your images for readability. (I use Adobe Scan myself).
* **Late homework policy:**
    - Any homework turned within the first hour of the deadline will be assessed a 20% penalty. (Note, it takes a few minutes to upload your homework/exam via Gradescope. So don't cut it too close to the deadline.)
    - Any homework turned after the first hour past the deadline will be assessed a 100% penalty (i.e. you won't receive credit for this homework, but if practical, your homework may still be graded).
    - To account for unusual circumstances, the bottom 20% of your homework will not count towards your grade.
    - I will only consider making an exception to the above late homework policy if you have documented personal emergencies lasting at least **18 days**.
* I recommend starting the homework early.
  Most students will not be able to do the homework in one evening.
* You may collaborate, use books and whatever resources you can find online in order to do the homework.
  However, you **must** write your solution up independently, **and** you must fully understand any solution you turn in.
  Turning in solutions you don't understand will be treated as a violation of academic integrity.
* In order to ensure academic integrity is maintained, I will call on some subset of students to explain their solutions to me outside class.
* New material will be developed through homework problems.
  If you are unable to solve a particular problem, be sure to ask me or your TAs about it, or look up the solutions after they are posted.
* Some homework problems will also appear on your exams *with a devious twist*.
  A through understanding of the solutions (even if you didn't come up with it yourself) will invariably help you.
  But knowledge of the solution without understanding will almost never help you.
* Nearly perfect student solutions may be scanned and hosted here, with your identifying information removed.
  If you don't want any part of your solutions used, please make a note of it in the margin of your assignment.

### Exams

* All exams are open book. You may use your notes, references or any online resources available.
* You may not, however, take assistance from other persons. This includes via email/messaging or posting on online discussion boards.
* You may take the exam at any time on the exam day (24 hours). You must not discuss the exam with anyone (even people outside the class) until the exam day ends.
* All exams are self proctored. You must record yourself using Zoom (audio, video, and screen), for the entire duration of the exam.
    - Start a new Zoom meeting. Share your screen (entire screen, not just a window). Record it to the cloud.
    - Be sure that the recording also shows you, either picture in picture, or separately. There are many settings in Zoom that change this, so please test it before the exam.
    - Your recording should start with you identifying yourself, and show you downloading the exam.
    - The screen recording should capture the entire screen, and can't have any time gaps; it should end with you uploading your exam on Gradescope.
    - You should stay within your webcams field of view for the entire exam.
    - If any other electronic device is used (tablets / phones), you must also take a screen recording of that device via Zoom for the entire duration of the exam.
    - Your recording must remain accessible to me the Zoom server for at least 6 months.
    - Additionally, you must download a copy of the recording, and upload it to the shared Box folder that will be setup for the exam.
    - Exams turned in without an accompanying recording will receive no credit.

{#
* All exams are closed book, in class.
* No calculators, computational aids, or internet enabled devices are allowed.
* The final time will be announced by the registrar
  [here](https://www.cmu.edu/hub/courses/exams/index.html).
  <span class='text-danger'>
  Be aware of their schedule before making your travel plans.
  </span>
-#}

### Academic Integrity

* All students are expected to follow the academic integrity standards outlined [here](https://www.cmu.edu/policies/student-and-student-life/academic-integrity.html).
* There will be zero tolerance for academic integrity violations, and any violation will result in an automatic `R`.
  Examples of academic integrity violations include (but are not limited to):
    - Not writing up solutions independently and/or plagiarizing solutions.
    - Turning in solutions you do not understand.
    - Receiving assistance from another person during an exam.
    - Providing assistance to another person taking an exam.
    - Discussing the exam with anyone during the exam day.
* All academic integrity violations will further be reported to the university, and the university may chose to impose an additional penalty.

### Grading

* Homework will count for 40% of your grade.
* The midterm will count for 20% of your grade
* The final will count for 40% of your grade.

### Accommodations for Students with Disabilities

If you have a disability and have an accommodations letter from the Disability Resources office, I encourage you to discuss your accommodations and needs with me as early in the semester as possible. I will work with you to ensure that accommodations are provided as appropriate. If you suspect that you may have a disability and would benefit from accommodations but are not yet registered with the Office of Disability Resources, I encourage you to contact them at <access@andrew.cmu.edu>.

### Student Wellness

As a student, you may experience a range of challenges that can interfere with learning, such as strained relationships, increased anxiety, substance use, feeling down, difficulty concentrating and/or lack of motivation. These mental health concerns or stressful events may diminish your academic performance and/or reduce your ability to participate in daily activities. CMU services are available, and treatment does work. You can learn more about confidential mental health services available on campus [here](http://www.cmu.edu/counseling). Support is always available (24/7) from Counseling and Psychological Services: 412-268-2922.


### Faculty Course Evaluations

At the end of the semester, you will be asked to fill out faculty course evaluations.
Please fill these in promptly, I value your feedback.
As incentive, if over 75% of you have filled out evaluations on the last day of class, then I will release your grades as soon as they are available.
If not, I will release your grades at the very end of the grading period.
