{%- if glob('pdfs/hw.pdf') -%}
* [[pdfs/hw.pdf|Homework]]
{%- elif glob('pdfs/hw-page*.pdf') -%}
* [[{{glob('pdfs/hw-page*.pdf')[0]}}|Current Assignment]]
    {%- if glob('pdfs/hw-prev.pdf') %}
* [[pdfs/hw-prev.pdf|Previous assignments]]
    {%- endif %}
{%- else -%}
Homework and solutions will be posted here when available.
{%- endif -%}
{%- if glob('auth/sol*') %}
* [[sol.md]]
{%- endif %}
{%- if glob('auth/*.csv') %}
* [[auth/grades.md|Your Grades]]
{%- endif %}
* [Gradescope](https://www.gradescope.com/courses/176487)
  <small>([[auth/gradescope.md|invitation code]].)</small>
