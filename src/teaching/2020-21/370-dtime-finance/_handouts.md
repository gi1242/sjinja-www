{%- from 'teaching/courses.j2' import old_teaching -%}
* [[auth/zoom.md]]
<div class='my-2'></div>
* [[lectures.md]]
* [[pdfs/dtime-fin-ann.pdf|Annotated Slides from class]]
* [[pdfs/dtime-fin.pdf|Compactified un-annotated slides from class]]
* [The LaTeX source for slides](https://gitlab.com/gi1242/cmu-math-370)
* [[auth/2019-kramkov-notes.pdf|2019 notes by Kramkov]]
<div class='my-2'></div>
* [[code.md]]
<div class='my-2'></div>
* Your [[pdfs/midterm1.pdf|Midterm 1]] and [[auth/midterm1-sol.pdf|solutions]].
* Your [[pdfs/midterm2.pdf|Midterm 2]] and [[auth/midterm2-sol.pdf|solutions]].
* Your [[pdfs/final.pdf|Final]] and [[auth/final-sol.pdf|Solutions]]
{#
* 2015 [[../../2015-16/268-multid-calc/pdfs/midterm1.pdf|Midterm 1]] and [[auth/2015-midterm1-sol.pdf|Solutions]]
* 2017 [[../../2017-18/268-multid-calc/pdfs/midterm1.pdf|Midterm 1]] and [[auth/2017-midterm1-sol.pdf|Solutions]]
* Your [[pdfs/midterm1.pdf|Midterm 1]] and [[auth/midterm1-sol.pdf|Solutions]]
* 2015 [[../../2015-16/268-multid-calc/pdfs/midterm2.pdf|Midterm 2]] and [[auth/2015-midterm2-sol.pdf|Solutions]]
* 2017 [[../../2017-18/268-multid-calc/pdfs/midterm2.pdf|Midterm 2]] and [[auth/2017-midterm2-sol.pdf|Solutions]]
* Your [[pdfs/midterm2.pdf|Midterm 2]] and [[auth/midterm2-sol.pdf|Solutions]]
* [[../../2018-19/269-vector-analysis/pdfs/lagrange.pdf|A note on Lagrange multipliers]]
* 2015 [[../../2015-16/268-multid-calc/pdfs/final.pdf|final]] and [[auth/2015-final-sol.pdf|Solutions]]
* 2017 [[../../2017-18/268-multid-calc/pdfs/final.pdf|final]] and [[auth/2017-final-sol.pdf|Solutions]]
* [[../../2015-16/268-multid-calc/pdfs/sample-questions.pdf|Sample Midterm 1 Questions]]
* [[../../2015-16/268-multid-calc/pdfs/sample-questions2.pdf|Sample Midterm 2 Questions]]
* [[../../2015-16/268-multid-calc/pdfs/sample-questions3.pdf|Sample integration Questions]]
-#}
