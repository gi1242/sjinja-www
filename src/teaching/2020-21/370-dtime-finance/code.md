title: Numerically pricing securities

Here are a few code snippets I wrote in connection to this class. Feel free to copy and modify them to suit your purposes.

All code I've written is in [Python](https://www.python.org/) as a [Jupyter Notebook](https://jupyter.org/) using [NumPy](https://numpy.org/) (numerical python libraries).
To run my code you will have to install these on your computer.
The websites above have installation instructions.

You are, of course, free to write code in your preferred language, or even use a spreadsheet.
My code is provided here just as an example.

* Pricing up-rebate options using the state process $Y = S$:
  [[code/up-rebate-using-just-S.html|view]]
  [[code/up-rebate-using-just-S.ipynb|download]].
* Pricing up-rebate options using the state process $Y = (S, M)$, where $S$ is the stock price, and $M$ is the running maximum:
  [[code/up-rebate-options.html|view]]
  [[code/up-rebate-options.ipynb|download]].
* Pricing American/European options (much faster code; but less general as it pre-supposes the domain):
  [[code/american-options.html|view]]
  [[code/american-options.ipynb|download]].
* A vote forecasting model:
  [[code/vote-forecasting.html|view]]
  [[code/vote-forecasting.ipynb|download]].
