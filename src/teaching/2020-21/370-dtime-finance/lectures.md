title: Videos of Lectures, and Notes

These are PDFs of everything I wrote on the screen during lectures, along with links to videos for every lecture that the recording worked.
(Note: Many office hours by me/your TAs have notes on the shared Google Drive folder instead of here.)

{% set videos = dict(

  l01='https://cmu.zoom.us/rec/share/2fBFMpbi-WRLUqPOx0zZXYwCPKi4eaa80ygX_vMMmkYmDuPPsikO4rUzJVKSyHM2',
  l02='https://cmu.zoom.us/rec/share/VvFATL39XeHJI4_upJ7xiReAtFpPRxETit9Ah-g5B2uAMp-0yl9kxONlN89ptnHm.X6UMuVflJXtOTucr',
  l03='https://cmu.zoom.us/rec/share/0LRYiboSeWrAPoKTz5mBnsG7QaNe9UkATM7uKmbA8R_iSspfcLkNgN2NMeEbBZDr.TT4HQMea-xR7lQ1G',
  l04='https://cmu.zoom.us/rec/share/oF5uYpWps-sz8PWLYDqPX1T1P0FCHYQ6_cChi8ii5kcCDj0aGimCqm6BsSJzXu0k.HbM8LTcgif89XVKH',
  l05='https://cmu.zoom.us/rec/share/EGOMxAH2zkH304nRXS--xcT1HExVXLxzQHv8YlJuYIQY3k09GSeZb3I_ybxcikOj.vXAs9eo7TkhtFFiA',
  l06='https://cmu.zoom.us/rec/share/lLWgag-NCW0wsuNIFAhUYFQQxqm5ebP3VlGHk9wfK0DIgKOXfRjIHsoej2spSmaG.__Pu2wj0uP1I_oRH',
  l07='https://cmu.zoom.us/rec/share/e6UhiM8Kx9d01Z99EYmV5VTxTGZQcvb5kULGlSxdOwfDblpc9dcXW8JnFaqXguGM.FvGqF2P_YxUX_rEs',
  l08='https://cmu.zoom.us/rec/share/LEa5IO4fMmGosVcCuQoEVBVsxLVzCEWYt7JyldjEJbo1ic2BjU1inqMAkZ_oM6dh.lvLOYJgJ-XhoMO1D',
  l09='https://cmu.zoom.us/rec/share/4jlJg140Hhnr2Pr1z5ctTGKyrIvVtp-uBj0IZIOWpXtrQ0Xiz57RxfE7qvB2bsdh.LvWY6lAp1dzHokCe',
  l10='https://cmu.zoom.us/rec/share/P4zPkTk3KOybgiob6rnKarkDvvgw6JzWNYrVN1aWEYDcRMGxet9TeZbKsua-o3jZ.-RRgnf6-R-g0Gk76',
  l11='https://cmu.zoom.us/rec/share/ghQTNiE2OgqxOAFW5lLf3r1I-u2OBFBYXuXuJZVdM2y2QM3eDIRehFCmvdavz3Wm.jHq8jHLCHwcedDE8',
  l12='https://cmu.zoom.us/rec/share/NiP2mJ7RpysRD1GID4shzPu1WV3uBVJ0qJMFtRWKZlVhByQtQfp6rCcri3OeFMHr.Q8QB-JfM9P3LiK1S',
  l13='https://cmu.zoom.us/rec/share/0SQO-22-EFSqNgWEcbziOwJ2jCYenervpj1aZVckNx4CrzbHhg7ReQG0yabmdyU.fDJvGRJwjdIGRoq_',
  l14='https://cmu.zoom.us/rec/share/RFJcrQld30W2R6oCxrt7AjtUvniBP9LMS6cj81Hc40x97stMOudErdl-iZAnrIdv.5poxxecvwtROef08',
  l15='https://cmu.zoom.us/rec/share/XqXjVM9D85whaKPITZg_cLNjY98Dz3EjL2d7pSTb199MY7ZonGpR20pnbPONsDUK.K5cRBfl0EhoGexkb',
  l16='https://cmu.zoom.us/rec/share/aVEAiWuqflyeDinf0fnBjQYiuLzocuqbGm_2ZJ52X9pYuXtg3URdhjtMO8oZy-VC.9Y4hLPzKZ5OA3ROF',
  l17='https://cmu.zoom.us/rec/share/T70aIhAZEuOAhEPoOHqXbHnbTbq7kcmz7SdwkA9lkcZBjczmEyQnjBf0mAYhvdjt.WSJEvn0kG5yeIi4K',
  l18='https://cmu.zoom.us/rec/share/J_aBtEte7kXDkwhj9lizTZ_lqqqXgrA_YM5mZHn7Bm-oX_ahGtS8NuJpWnCldBzX.K2fpqemVCwuh2k-u',
  l19='https://cmu.zoom.us/rec/share/YN5vHnpGvbcrk7TX49PZYr74JraLveK136gs_K64l4Dn5Vij3dg7-LrL65McKJzY.rSjVC5KDskyQMpPd',
  l20='https://cmu.zoom.us/rec/share/gvaIMOEEG5MFSLOoCy_l7FRqR_Co1p01prOo-kBOi4r_Krx3vJfUL9I6YXfbe93t.cNn9gkxUxBnzi89S',
  l21='https://cmu.zoom.us/rec/share/7emui88mArFVC0bIjlW3tLkYTvgx35zOONs4HX3e8nUFBDEbBOGxXMLj1wELKbO8.rp_k8FNDgEiO57zZ',
  l22='https://cmu.zoom.us/rec/share/xREytZdm5sNQ4o5oaSdlB2xoE0Vl8lCYUcR0LHTG5gIT85oo4SNOI9riNBcOvYf1.0cQX2Tn-GogUu7CE',
  l23='https://cmu.zoom.us/rec/share/XqgVDRwcOT9JqA4ccH9cGRiqUgaondaX2jW9xprqrbGoIUQzBhRmwKbwbvm1IBHl.NC-y2WLf7G7mlWuQ',
  l24='https://cmu.zoom.us/rec/share/9J42Y66F5KQ6TvnOq9FDR63Q7DfAC4bjjYlNdktYIlqPwYCycIcjk37fROmkB3im.PyKQ_BVvWoV8Oszw',
  l25='https://cmu.zoom.us/rec/share/k2-LX_rX_HELccQEERyASTlKRkPeFc_rw-5IbYbyteQH2nQpwCivPGcp3CB3F9nY.xcGijRtySdYxBowb',
  l26='https://cmu.zoom.us/rec/share/s3tJjHLgc3ayfc5BdEo8pfr8La4FPGUcF6KZdlg6sbIolRs3HfunP8QY3fchiP3j.47eoL8fR1BrcgQW9',
  l27='https://cmu.zoom.us/rec/share/amd9uIZkXXOStEC8TB5XUcFIWFUkKpgRvKKjJAZ-7bzmMwzz1l2VMoXvd535kR2S.18wyK1Jn2yPITvGU',
  l28='https://cmu.zoom.us/rec/share/TYH9srnWc3HiBtu_dZ1TpCxvVbFf5asnNIsMYQIhi3ahxQxBXhh37ECJtOFIc7T-.BR5TluKAtemKg-YG',
  l29='https://cmu.zoom.us/rec/share/_wF7SYlQUTKiBq_f4lpxlRzzpqVfQbVMsMSKnh4d9-CF-I7cvTJCFZwhOxnYK51b.Ou7eEmKtkOo9I8rU',
  l30='https://cmu.zoom.us/rec/share/Vlzqy1H4o2cMSE9A5yP8FXLC_wOcOHFsZ-c0MSSnACR3-uL4RkMTo3rEXWOXwmnM.gXidVFydeMKRPo4p',
  l31='https://cmu.zoom.us/rec/share/eq2YPM_ojvagwGW0mgFRsBH0WIkQzY3FuD3XJuUReidqxOLMa4W9YA3khLr8V2Ps.dg8R7V3xj2nQRjfG',
  l32='https://cmu.zoom.us/rec/share/pdK8rzw48zDeLpVfN1wYHvuKdWCunvASvTjt2izULCwiu_S7L6VxIhdwyjwTsT9a.zN4jTQB2D9lYFd4N',
  l33='https://cmu.zoom.us/rec/share/PejtaVeYC4AGm4RHP2WKwCgzWFAXytoYNm9yZdtPfn0p8gOxUI9-4fYcywkXG-jg.7_-swkY_9YTEKOt5',
  l34='https://cmu.zoom.us/rec/share/fLd_Z-5gBr1D6YAP7J-VsTDKv5KOMf3LiHSk5QKdfgTVre0-9dn_jGXatnR3hgHW.DhwFvvARa6ErOnum',
  l35='https://cmu.zoom.us/rec/share/wCYdcHSi37iA5zslphITxQ-2l4Vb-Fk-_GyHdljkerIx7RqQmK6DxzrNUEdByURZ.9b48sp5Rp2_F_KxR',
  l36='https://cmu.zoom.us/rec/share/AxjH3_CvSJNbd10HEPVJu0sZ9T3TtUnlt_xpAE4d_wEoEiawpgLtrCucv4t0PFVa.1ejYQvwybYcPoOTB',
  l37='https://cmu.zoom.us/rec/share/kZOZGSFLYxyfNHraaVSR7VB8F4Hw4YtfCO73_71WX4gqKB5-B3mYyk6aQM9mGL2u.14NIGGgTlH8BdPcL',
  l38='https://cmu.zoom.us/rec/share/9vjVo9xUuqlNu1Oy9yAkS4xyXyDsfH0kVYCKF9gIH2027pYdbb-KYNhhcYn1bDAf.UcZSt7O0_qoAErKj',

  o02='https://cmu.zoom.us/rec/share/JervwXl0CMARljG-SLUu9ysZ2lStA4cInrzyiRAlzinM0slpRxf-Psr5Ti-9IawK._WGkre57pzYBowCK',
  o04='https://cmu.zoom.us/rec/share/011itRMqCRkVUmLs1xAnSKj7yrNpo-MsgScdtm53WZEYrio5Z6gJZa37qiKIG-ZS.5RYTnSBUMpeVzUUB',
  o08='https://cmu.zoom.us/rec/share/bWQIrbN5bJEFPrYj93ioj27bx125cWPcXXh_pnW8LnWvlQaYAm-68z9Agfwv8Hcp.Ga_V-9DRmtR-vp2G',
  o09='https://cmu.zoom.us/rec/share/6IOT7DOLcpymvGRwger7oOnBeyvGr2vPPglNWkwG5BtXFn35Ty-MbRhrkxqnpxQd.KFJTFYREaHnfEZ4n',
  o10='https://cmu.zoom.us/rec/share/RoQfdLmlE-fXYFQmhX01kc6vZ2KJR6rcg8c96BaFCzzCZ1y7-_DaMqSK576xHJFg.-q6IseQOU4Dcm0MA',
  o12='https://cmu.zoom.us/rec/share/rCnh6QqOZG8J7b4Bj-9Y4IBYSKo7CXHIW21Zaa9apPAQxl_6hafJ-FvnAAuPcRd-.nYehL8EdH9TeLkDV',
  o13='https://cmu.zoom.us/rec/share/A7gSMgqDm1tThNBeNeYk05TRSMjeLeaLVCB4PMh_uGjfWFEnwXbs6u5RssMUZEtr.CTN3w_RjpFdwdvKd',
  o14='https://cmu.zoom.us/rec/share/TIjJWN1Nz-MXxoMxJRn2e1g6yWCSVJJRCRpgOJnNyn380L6tl7q_Zuo7PGX5rzJw.cowd3GQ9UVxJqAOW',
  o15='https://cmu.zoom.us/rec/share/9k_JhuDEkxFAbzV3MI6_UYQiDgDvckV2ooe0ExK-MbXz7O8kcEqdUwnhDSEtOuTh.eFZVhuGf--YOfZdg',
  o16='https://cmu.zoom.us/rec/share/jPbGlswU9KJy0_FO1QpJyNgtkMzSN7C50b6vGzkhkg2HX6ogib9FA6s97u7pDFc.9DaiUnfl7bMW7I61',
  o19='https://cmu.zoom.us/rec/share/UeV9n32Ery63FlqMxDqfRZLSxbnj9-sBQe7NuAwT44rAiVV9a3ZxHd2agJLiw3pr.DNnthvzL-0JNwP5m',
  o21='https://cmu.zoom.us/rec/share/NIpY8x2W5DS3NV_DPNOMaHBCx5D7aPa_YVp6CiEVrhpZZkZ-NT1k9awxO6KDCgQB.Y3sJthtB91Q6F0OL',
  o22='https://cmu.zoom.us/rec/share/UYlnBXQB7WUBtO2cjslTvnueXJIS6RnBHdQs5NAoFS56H9v8tcwwaWZkDuFvFS-3.3ctHXZc74NmWaVf5',
  o23='https://cmu.zoom.us/rec/share/uPV2sF4_EiCG6El6-lNxXuJZhaFnfNcnBQEXzRmouUHrrW9BqDLAIfvqsS3AoPXw.PEq265e10vSQqGdO',
  o24='https://cmu.zoom.us/rec/share/T90Zh_5KlUcOc_waAYvUuDUJQOjC5Mzh1b4lJByvthH-SvllT2fIIf_ttrKg59tW.6oz26tj60fiNof7l',
  o26='https://cmu.zoom.us/rec/share/fjecDmndRRG1jJ2D7mCxjzLR6i3hSpLNCNZAXzXJpKNjhng0J9L_wa7ekgxKk_te.NksEUb3-5WprS3eq',
  o27='https://cmu.zoom.us/rec/share/OEV6srPcVU7B-K354ob9rUNYUFYpHYQMdulA-mznmsZuZ98a_Lkph7PoVMGEAgUU.yHjrCm8CVV2oL4vU',
  o28='https://cmu.zoom.us/rec/share/ODn-3zDreYGmg1ahyGvTbdBcBY-PI7B-hGc9VnjjmhcfitAPGqPXI41jKzizMHjQ.oMx7iWYHEo2xixMA',

) -%}
{% for f in glob('pdfs/lec/20*-?[0-9]*.pdf') | sort -%}
  {% set fn = f | replace( 'pdfs/lec/', '') | replace( '.pdf', '') -%}
  {% set date = fn[0:8] -%}
  {% set type = fn[9:] -%}
  {% if loop.first -%}
    <table class='table'>
      <thead>
        <tr>
          <th scope='col'>Date</th>
          <th scope='col'>Event</th>
          <th scope='col'>PDF</th>
          <th scope='col'>Video</th>
        </tr>
      </thead>
  {% endif -%}
      <tbody>
        <tr>
          <td>{{date[0:4]}}-{{date[4:6]}}-{{date[6:8]}}</td>
          <td>
            {%- if type[0] == 'r' -%}
              Recitation
            {%- elif type[0] == 'o' -%}
              Office hour
            {%- else -%}
              Lecture
            {%- endif -%}
            {{' #' ~ type[1:]}}
          </td>
          <td><a href='{{get_link(f)}}'>🗐</a></td>
          {%- if videos[type] %}
            <td><a href='{{videos[type]}}'>🎥</a></td>
          {%- endif %}
        </tr>
  {% if loop.last -%}
      </tbody>
    </table>
  {% endif -%}
{% else -%}
    * *Notes will be posted as the semester progresses.*
{% endfor %}
