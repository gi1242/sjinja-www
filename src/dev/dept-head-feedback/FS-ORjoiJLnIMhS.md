layout: md-base.j2
title: Department Head Search Anonymous Feedback

Any comments entered below will be emailed to the entire committee anonymously (your IP address will **not** be included with your comment).

<div id='comment-failed' class='alert alert-danger d-none'>
  Sorry. There was an error submitting your comment. Please try again, or contact me (Gautam) if the problem persists.
</div>
<div id='comment-sending' class='alert alert-info d-none'>
  Sending comment; please wait.
</div>
<div id='comment-success' class='alert alert-success d-none'>
  Thanks. Your comment was successfully emailed to the committee.
</div>

<form id='comment-form' role='form' method='post'
    action='javascript:submit_form()'>
  {%- set category = basename.split('-')[0] %}
  <input type='hidden' name='category' value='{{category}}'>
  <div class='form-group my-3'>
    <label class='font-weight-bold' for="comment">Do you have any comments on the department head candidates?</label>
    <textarea class="form-control my-1" name="comment"
      placeholder="Enter your comments here"
      rows='10'></textarea>
  </div>
  <div class='form-group my-3'>
    <label class='font-weight-bold' for="interview">Do you have any comments on the interview process?</label>
    <textarea class="form-control my-1" name="interview"
      placeholder="Enter your comments here"
      rows='10'></textarea>
  </div>

  <div class="text-right my-1">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>

**Note:** You can confirm anonymity, by viewing the source of this webpage, and the corresponding `python` script on the math cluster.

<script>
  function submit_form() {
    $('#comment-failed,#comment-form,#comment-success').addClass('d-none');
    $('#comment-sending').removeClass( 'd-none' );

    var failfn = function(data) {
      $('#comment-form,#comment-sending').addClass('d-none');
      $('#comment-failed,#comment-form').removeClass( 'd-none' );
      $('#comment-failed').append('<pre>' + data + '</pre>');
    };

    $.post( "{{get_link('/cgi-bin/dhead-email.py')}}",
        $('#comment-form').serializeArray() )
      .done( function( data ) {
          if( data.trim() == "OK" ) {
            $('#comment-failed,#comment-form,#comment-sending')
              .addClass('d-none');
            $('#comment-success').removeClass( 'd-none' );
          }
          else failfn(data);
        }
      )
      .fail( failfn );
  }
</script>
