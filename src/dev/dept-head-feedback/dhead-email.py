#! /usr/bin/python3
import cgi
from email.mime.text import MIMEText
import smtplib

form = cgi.FieldStorage()

print( "Content-type: text/plain;\n" )

comment = form.getvalue( 'comment', 'No comment' ).strip()
interview = form.getvalue( 'interview', 'No comment' ).strip()
category = form.getvalue( 'category', 'Unknown' ).strip()

msg = MIMEText("""\
Comments on candidates:

%s

-----
Comments on the interview process:

%s
""" % (comment, interview),
    _charset='utf-8'
)

to = [ 'gautam@math.cmu.edu',
    'tbohman@math.cmu.edu', 
    'sdodelso@andrew.cmu.edu', 
    'fonseca@andrew.cmu.edu',
    'martinl@andrew.cmu.edu',
    'clintonc@andrew.cmu.edu',
    'dmihai@andrew.cmu.edu',
    'cgilchri@andrew.cmu.edu' ]
frm = 'Anonymous <%s>' % to[0]

msg['Subject'] = 'Department head anonymous feedback (%s)' % category
msg['To'] = ', '.join(to)
msg['From'] = frm

s = smtplib.SMTP('localhost')
print( s.sendmail( frm, to, msg.as_string() ) or "OK" )
s.close()
