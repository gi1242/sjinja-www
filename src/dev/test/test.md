title: Various Miscalaneous Tests in Markdown
layout: md-nav-right.j2

## Iteration

{% set a = {'a': 1, 'b': 2} %}
{% for i in a -%}
* `i={{i}}`
{% endfor %}

## Filename and includes

``` yaml
title       : {{title}}
dirname     : {{dirname|e}}
basename    : {{basename|e}}
filesdir    : {{filesdir|e}}
name        : {{name}}
name[:-3]   : {{name[:-3]}}
1 + 1       : {{1 + 1}}
```

## WikiLinks

* `auth/test`: [[auth/test]]
* `test`: [[test]]
* `/test`: [[/test]]
* `test.md`: [[test.md]]

## Glob

All files matching `*.md`:

    {{glob('*.md') | sort}}

Iteration over a glob:

{% for f in glob( '*.md') | sort -%}
* [[{{f}}|{{get_meta( f, 'title' )|default(f, true) }}]]
{% endfor %}


### Filtering tests

* {{ glob( '*.md' ) | search( '^_' ) }}

### Few custom macros in `share/js/mathjax-config.js`:

* `\eps`: $\eps$
* `\suchthat`: $\suchthat$
* `\st`: $\st$
* `\dv`: $\dv$
* `\curl`: $\curl$
* `\defeq`: $\defeq$
